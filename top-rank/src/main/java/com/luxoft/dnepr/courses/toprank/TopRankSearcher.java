package com.luxoft.dnepr.courses.toprank;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/26/13
 * Time: 6:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopRankSearcher {
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;
    private static final int DEFAULT_CONTENT_THREADS_NUMBER = 4;
    private static final long DEFAULT_SLEEP_THREAD_TIME_IN_MS = 100L;

    public TopRankResults execute(List<String> urls) {
        return execute(urls, DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
    }

    /**
     * метод execute принимает список url адресов, считывает их контент, а затем анализирует его, возвращая TopRankResults
     *
     * @param urls
     * @param dampingFactor
     * @param numberOfLoopsInRankComputing
     * @return
     */
    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        TopRankExecutor executor = new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);

        Map<String, String> urlContent = new ConcurrentHashMap<>();

        urlContent = getContentFromUrlsConcurrently(urls, urlContent);

        TopRankResults results = executor.execute(urlContent);

        return results;
    }

    /**
     * Function creates ThreadPool and run connection to hosts from
     * urls,
     * @param urls list of Urls in String
     * @param urlContent in-out for content
     * @return urlContent
     */
    private Map<String, String> getContentFromUrlsConcurrently(List<String> urls,
                                                               Map<String, String> urlContent) {

        ExecutorService pool = Executors.newFixedThreadPool(DEFAULT_CONTENT_THREADS_NUMBER);

        for (String strUrl : urls) {
            pool.execute(new GetContentFromNet(strUrl, urlContent));
        }
        //trying to shutdown all threads
        pool.shutdown();
        while (!pool.isTerminated()) {
            try {
                Thread.sleep(DEFAULT_SLEEP_THREAD_TIME_IN_MS);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return urlContent;
    }

    /**
     * Runnable class that connects to the host by url
     * and gets its content
     *
     */
    private class GetContentFromNet implements Runnable {
        private String strUrl;
        private Map<String, String> urlContent;

        public GetContentFromNet(String strUrl, Map<String, String> urlContent) {
            this.strUrl = strUrl;
            this.urlContent = urlContent;
        }

        private Map<String, String> getUrlContent() {
            return urlContent;
        }

        @Override
        public void run() {
            StringBuilder content = new StringBuilder("");
            try {
                URL url = new URL(strUrl);
                URLConnection connection = url.openConnection();
                connection.connect();
                if (connection.getContentType().equals("text/html")) {
                    Scanner in = new Scanner(connection.getInputStream());
                    while (in.hasNextLine()) {
                        content.append(in.nextLine());
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            if (this.urlContent != null)
                this.urlContent.put(this.strUrl, content.toString());
        }
    }
}