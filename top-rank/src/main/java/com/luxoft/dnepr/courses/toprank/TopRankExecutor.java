package com.luxoft.dnepr.courses.toprank;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;

    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();

        buildGraph(urlContent, results.getGraph());

        buildReverseGraph(urlContent,results.getReverseGraph());

        buildIndex(urlContent, results.getIndex());

        buildRanks(results.getGraph(), results.getRanks());

        return results;
    }

    /**
     * create map of links and their ranks
     * @param graph
     * @param ranks
     * @return
     */
    private Map<String, Double> buildRanks(Map<String, List<String>> graph, Map<String, Double> ranks) {
        //calculating first ranks
        for (String url : graph.keySet()) {
            if (!ranks.containsKey(url)) {
                ranks.put(url, new Double(1d / new Double(graph.keySet().size())));
            }
        }
        //calculating in loop Rank
        int loop = numberOfLoopsInRankComputing;
        while (loop > 0) {
            Map<String, Double> oldRank = new HashMap<>(ranks);

            for (String url : graph.keySet()) {
                Double rankValue = (1d - dampingFactor) / graph.keySet().size() + getBackReferenceWage(graph, oldRank, url);
                ranks.put(url, rankValue);

            }
            loop--;
        }


        return ranks;
    }

    /**
     * Helper function that return
     * suppose for the next iteration rank for the page C calculates like
     * rank(C) = (1 - dampingFactor) / numberOfPages
     * + dampingFactor * previous_rank(A) / 2
     * +dampingFactor * previous_rank(B) / 1
     *
     * so wage for the page C will be
     * rank(C) = (1 - dampingFactor) / numberOfPages + <b>wage</b>
     *
     *
     *
     *
     * @param graph link to the graph of backlinks
     * @param oldRanks link to the previous rank map
     * @param url - calculating wage for url
     * @return wage
     */
    private Double getBackReferenceWage(Map<String, List<String>> graph, Map<String, Double> oldRanks, String url) {
        Double wage = new Double(0);
        if (graph.get(url).isEmpty())
            return 0d;
        for (String backLink : graph.get(url)) {
            wage += dampingFactor * oldRanks.get(backLink) / getReferenceCount(graph, backLink);
        }
        return wage;
    }

    /**
     * Get references amount from <b>url</b> to another pages
     * EXAMPLE: pageA has links to pageB and PageC, then function must return 2;
     * @param graph with backlinks
     *
     *              graph = {
    * "http://no_address/A.html" : [],
    * "http://no_address/B.html" : ["http://no_address/A.html"],
    * "http://no_address/C.html" : ["http://no_address/A.html", "http://no_address/B.html"]
    }
     *
     * @param url http://no_address/A.html
     * @return
     */
    public Long getReferenceCount(Map<String, List<String>> graph, String url) {
        Long counter = new Long(0);
        for (String key : graph.keySet())
            for (String link : graph.get(key)) {
                if (link.equals(url))
                    counter++;
            }
        return counter;
    }

    /**
     * Build Index
     *
     *
     * index = {
     *"<html>" : ["http://no_address/A.html", "http://no_address/B.html", "http://no_address/C.html"],
     *"</body>" : ["http://no_address/A.html", "http://no_address/B.html", "http://no_address/C.html"],
     *"<h1>Page" : ["http://no_address/A.html", "http://no_address/B.html", "http://no_address/C.html"],
     *"C</h1>" : ["http://no_address/C.html"],
     *...
     *}
     * @param urlContent
     * @param index
     * @return
     */
    private Map<String, List<String>> buildIndex(Map<String, String> urlContent, Map<String, List<String>> index) {
        for (String url : urlContent.keySet()) {
            String content = urlContent.get(url);
            String[] words = content.split("[\t\n\\s]");
            for (String word : words) {
                if (index.containsKey(word)) {
                    index.get(word).add(url);
                } else {
                    ArrayList<String> urlList = new ArrayList<>();
                    urlList.add(url);
                    index.put(word, urlList);
                }
            }
        }
        return index;
    }

    /**
     * create graph
     * graph = {
     *"http://no_address/A.html" : [],
     *"http://no_address/B.html" : ["http://no_address/A.html"],
     *"http://no_address/C.html" : ["http://no_address/A.html", "http://no_address/B.html"]
     *}
     * @param urlContent
     * @param graph
     * @return
     */
    private Map<String, List<String>> buildGraph(Map<String, String> urlContent, Map<String, List<String>> graph) {
        for (String url : urlContent.keySet()) {
            List<String> backUrls = new ArrayList<>();
            for (String backUrl : urlContent.keySet()) {
                if (!url.equals(backUrl)) {
                    String content = urlContent.get(backUrl);
//                    Pattern p = Pattern.compile("<a href=\"" + url + "\">(?s).*</a>");
                    Pattern p = Pattern.compile("<a href=\"" + url + "\">(?s).*</a>");
                    Matcher m = p.matcher(content);
                    if (m.find()) {
                        backUrls.add(backUrl);
                    }
                }
            }
            graph.put(url, backUrls);
        }
        return graph;
    }

    /**
     * Helper function that builds reverse graph -
     * scan all links on the page for each pages, and returns them fo each page
     * see  buildGraph function
      * @param urlContent
     * @param reverserGraph
     * @return
     */
    private Map<String, List<String>> buildReverseGraph(Map<String, String> urlContent, Map<String, List<String>> reverserGraph) {

        for (String url : urlContent.keySet())
        {
            List<String> straightUrls = new ArrayList<>();

            String content = urlContent.get(url);
            Pattern p = Pattern.compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Matcher m = p.matcher(content);
            while (m.find()) {
                straightUrls.add(content.substring(m.start(), m.end()));
            }

            reverserGraph.put(url,straightUrls);
        }

        return reverserGraph;

    }

}