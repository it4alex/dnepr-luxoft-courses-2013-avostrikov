package com.luxoft.dnepr.courses.toprank;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TopRankExecutor_my_Test {

    private static final double ERROR = 0.000001;

    @Test
    public void testExecute() throws Exception {
        TopRankExecutor executor = new TopRankExecutor(0.8, 2);
        Map<String, String> urlContent = new HashMap();
        String seedUrl = "http://no_address/A.html";
        urlContent.put("http://no_address/A.html",
                "<html>\n"+
                "<body>\n"+
                "<h1>Page A</h1>\n"+
                "<a href=\"http://no_address/B.html\">B</a>\n"+
                "<a href=\"http://no_address/C.html\">C</a>\n"+
                "</body>\n"+
                "</html>");
        urlContent.put("http://no_address/B.html",
                        "<html>\n"+
                        "<body>\n"+
                        "<h1>Page B</h1>\n"+
                        "<a href=\"http://no_address/C.html\">C</a>\n"+
                        "</body>\n"+
                        "</html>");

        urlContent.put("http://no_address/C.html",
                "<html>\n"+
                        "<body>\n"+
                        "<h1>Page C</h1>\n"+
                        "</body>\n"+
                        "</html>");

        TopRankResults results = executor.execute(urlContent);
        System.out.println(results);

        Assert.assertEquals(0, results.getGraph().get("http://no_address/A.html").size());
        Assert.assertEquals(1, results.getGraph().get("http://no_address/B.html").size());
        Assert.assertEquals(2, results.getGraph().get("http://no_address/C.html").size());

        Assert.assertEquals(3, results.getIndex().get("<html>").size());
        Assert.assertEquals(3, results.getIndex().get("<body>").size());

       Assert.assertTrue(executor.getReferenceCount(results.getGraph(),"http://no_address/A.html").equals(2L));
       Assert.assertTrue(executor.getReferenceCount(results.getGraph(),"http://no_address/B.html").equals(1L));
       Assert.assertTrue(executor.getReferenceCount(results.getGraph(),"http://no_address/C.html").equals(0L));
       Assert.assertEquals(0.0666667d, results.getRanks().get("http://no_address/A.html"), ERROR);
       Assert.assertEquals(0.09333338d, results.getRanks().get("http://no_address/B.html"), ERROR);
       Assert.assertEquals(0.2533333d, results.getRanks().get("http://no_address/C.html"), ERROR);
    }
}
