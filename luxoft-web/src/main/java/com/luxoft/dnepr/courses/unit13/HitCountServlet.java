package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/9/13
 * Time: 2:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class HitCountServlet extends HttpServlet {
    private static AtomicLong counter = new AtomicLong(0);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        counter.incrementAndGet();
        StringBuffer content = new StringBuffer("{\"hitCount\": ");
        content.append(counter).append("}");
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json; charset=utf-8");
        response.setContentLength(content.toString().getBytes().length);
        PrintWriter writer = response.getWriter();
        writer.println(content.toString());
        writer.close();
    }
}
