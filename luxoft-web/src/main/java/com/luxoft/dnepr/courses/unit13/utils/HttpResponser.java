package com.luxoft.dnepr.courses.unit13.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 4:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class HttpResponser {
    private HttpResponser() {
    }

    public static void writeResponse(HttpServletResponse response, StringBuilder content) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.println(content.toString());
        writer.close();
    }

    public static void setResponseParameters(HttpServletResponse response, StringBuilder content, int status) {
        response.setStatus(status);
        response.setContentType("text/html; charset=utf-8");
        response.setContentLength(content.toString().getBytes().length);
    }

}
