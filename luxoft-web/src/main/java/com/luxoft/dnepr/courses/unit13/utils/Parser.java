package com.luxoft.dnepr.courses.unit13.utils;

import com.luxoft.dnepr.courses.unit13.entity.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 1:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class Parser {
    private Parser() {
    }

    public static Set<User> parseJsoin(String json) {
        Set<User> users = new HashSet<User>();
        json = json.substring(json.indexOf("{") + 1, json.indexOf("}"));
        String[] pairs = json.split(",");
        for (String pair : pairs) {
            int i = 0;
            String[] keyValues = pair.split(":");
            i = keyValues[0].indexOf('\"');
            String login = keyValues[0].substring(++i,
                    keyValues[0].indexOf("\"", ++i));

            i = keyValues[1].indexOf('\"');
            String pwd = keyValues[1].substring(++i,
                    keyValues[1].indexOf("\"", ++i));

            User user = new User(login, pwd);
            users.add(user);
        }
        return users;
    }
}
