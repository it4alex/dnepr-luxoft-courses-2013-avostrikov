package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/9/13
 * Time: 2:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class DummyServlet extends HttpServlet {
    private static final String ILLEGAL_PARAMETERS = "{\"error\": \"Illegal parameters\"}";
    private static Map<String, String> mockDB = new ConcurrentHashMap();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("{");
        for (Map.Entry<String, String> element : mockDB.entrySet()) {
            content.append(String.format("{\"name\" : \"%1$s\", \"age\" : \"%2$s\"}", element.getKey(), element.getValue()));
        }
        content.append("}");
        setResponseParameters(response, content, HttpServletResponse.SC_OK);
        writeResponse(response, content);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        String name = request.getParameter("name");
        String age = request.getParameter("age");

        if (!isValidInputParameters(name, age)) {
            content.append(ILLEGAL_PARAMETERS);
            setResponseErrorParameters(response, content);
            writeResponse(response, content);
        }

        if (!mockDB.containsKey(name)) {
            content.append(String.format("{\"error\": \"Name ${%1$s} does not exist\"}", name));
            setResponseErrorParameters(response, content);
            writeResponse(response, content);

        } else {
            mockDB.put(name, age);
            setResponseParameters(response, content, HttpServletResponse.SC_ACCEPTED);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder content = new StringBuilder("");
        String name = request.getParameter("name");
        String age = request.getParameter("age");

        if (!isValidInputParameters(name, age)) {
            content.append(ILLEGAL_PARAMETERS);
            setResponseErrorParameters(response, content);
            writeResponse(response, content);
        }

        if (mockDB.containsKey(name)) {
            content.setLength(0);
            content.append(String.format("{\"error\": \"Name ${%1$s} already exists\"}", name));
            setResponseErrorParameters(response, content);
            writeResponse(response, content);

        } else {
            mockDB.put(name, age);
            setResponseParameters(response, content, HttpServletResponse.SC_ACCEPTED);

        }
    }

    private void writeResponse(HttpServletResponse response, StringBuilder content) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.println(content.toString());
        writer.close();
    }

    private void setResponseParameters(HttpServletResponse response, StringBuilder content, int status) {
        response.setStatus(status);
        response.setContentType("application/json; charset=utf-8");
        response.setContentLength(content.toString().getBytes().length);
    }

    private void setResponseErrorParameters(HttpServletResponse response, StringBuilder content) {
        setResponseParameters(response, content, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    private boolean isValidInputParameters(String name, String age) {
        return name == null || name.equals("") ||
                age == null || age.equals("");
    }
}
