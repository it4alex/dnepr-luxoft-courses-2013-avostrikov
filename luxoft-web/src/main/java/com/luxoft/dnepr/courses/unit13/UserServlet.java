package com.luxoft.dnepr.courses.unit13;

import com.luxoft.dnepr.courses.unit13.utils.ContentGenerator;
import com.luxoft.dnepr.courses.unit13.utils.HttpResponser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 3:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        if (request.getSession(false) != null)
        {
            String login = (String) request.getSession().getAttribute("login");
            content.append(ContentGenerator.getWelcomePage(login));
            HttpResponser.setResponseParameters(response, content, response.SC_OK);
            HttpResponser.writeResponse(response, content);
        } else response.sendRedirect("index.html");

    }
}
