import com.luxoft.dnepr.courses.unit13.entity.User;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/9/13
 * Time: 3:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class DummyTest {
    @Test
    public void dummyTest() {
        System.out.println(String.format("{\"error\": \"Name ${%1$s} already exists ${%2$s}\"}", "HELLO", "THERE"));
    }

    @Test
    public void checkJson() {
        String jsonValue =
                "{\"login1\": \"password1\", \"login2\": \"password2\", \"login3\": \"password3\"}";
        Set<User> users = new HashSet<User>();
        jsonValue = jsonValue.substring(jsonValue.indexOf("{")+1, jsonValue.indexOf("}"));
        String[] pairs = jsonValue.split(",");
        for (String pair : pairs) {
            int i=0;
            String[] keyValues = pair.split(":");
            i = keyValues[0].indexOf('\"');
            String user = keyValues[0].substring(++i,
                    keyValues[0].indexOf("\"", ++i));

            i = keyValues[1].indexOf('\"');
            String pwd = keyValues[1].substring(++i ,
                    keyValues[1].indexOf("\"", ++i));

            System.out.println(user + " " + pwd);


        }


    }
}
