package com.luxoft.dnepr.courses.unit15.servlet;

import com.luxoft.dnepr.courses.unit15.entity.SessionData;
import com.luxoft.dnepr.courses.unit15.listener.Constants;
import com.luxoft.dnepr.courses.unit15.utils.ContentGenerator;
import com.luxoft.dnepr.courses.unit15.utils.HttpResponser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 3:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class SessionDataServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        SessionData data = new SessionData();

        data.setActiveSessions(((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).get());
        data.setActiveSessionsAdmins(((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE)).get());
        data.setActiveSessionsUsers(((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE)).get());

        data.setTotalHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_REQUESTS)).get());
        data.setTotalGetHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_GET_REQUESTS)).get());
        data.setTotalPostHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_POST_REQUESTS)).get());
        data.setTotalOtherHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_OTHER_REQUESTS)).get());

        content.append(ContentGenerator.getStatistics(data));
        HttpResponser.setResponseParameters(response, content, response.SC_OK);
        HttpResponser.writeResponse(response, content);
    }
}
