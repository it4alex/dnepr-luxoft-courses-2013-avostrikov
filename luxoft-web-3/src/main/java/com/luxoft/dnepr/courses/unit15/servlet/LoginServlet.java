package com.luxoft.dnepr.courses.unit15.servlet;

import com.luxoft.dnepr.courses.unit15.entity.User;
import com.luxoft.dnepr.courses.unit15.utils.ContentGenerator;
import com.luxoft.dnepr.courses.unit15.utils.HttpResponser;
import com.luxoft.dnepr.courses.unit15.utils.Parser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 2:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginServlet extends HttpServlet {
    public static final boolean NO_ERROR = false;
    public static final boolean HAS_ERROR = true;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        content.append(ContentGenerator.getIndexPage(NO_ERROR));
        HttpResponser.setResponseParameters(response, content, response.SC_OK);
        HttpResponser.writeResponse(response, content);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        String login = request.getParameter("login");
        String password = request.getParameter("pwd");
        User user = new User(login, password);

        if (isRegisteredUser(user)) {
            HttpSession session = request.getSession();
            session.setAttribute("login", request.getParameter("login"));
            session.setAttribute("role", getUsersRole(request.getParameter("login")));
            response.sendRedirect("/myFirstWebApp/user");
        } else {
            content.append(ContentGenerator.getIndexPage(HAS_ERROR));
            HttpResponser.setResponseParameters(response, content, response.SC_OK);
            HttpResponser.writeResponse(response, content);
        }
    }

    private boolean isRegisteredUser(User newUser) {
        String jsonUsers = getServletContext().getInitParameter("users");
        Set<User> users = Parser.jsonToUsers(jsonUsers);
        return users.contains(newUser);
    }

    private String getUsersRole(String login) {
        String jsonUsers = getServletContext().getInitParameter("users");
        Set<User> users = Parser.jsonToUsers(jsonUsers);
        for (User user : users) {
            if (user.getLogin().equals(login))
                return user.getRole();
        }
        return "";
    }
}
