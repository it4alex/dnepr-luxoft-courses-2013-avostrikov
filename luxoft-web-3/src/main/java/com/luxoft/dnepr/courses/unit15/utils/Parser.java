package com.luxoft.dnepr.courses.unit15.utils;
import com.luxoft.dnepr.courses.unit15.entity.User;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 1:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class Parser {
    private Parser() {
    }

    public static Set<User> jsonToUsers(String json) {
        Set<User> users = new HashSet<User>();
        json = json.replaceAll("\\s", "");
        String[] elements = json.split("\\},\\{");
        for (String element : elements) {
            String[] partsOfElemet = element.replaceAll("[\\}\\{\\\"]+", "").split(",");
            String login = partsOfElemet[0].replaceAll("[\\w]+:", "");
            String pass = partsOfElemet[1].replaceAll("[\\w]+:", "");
            String role = partsOfElemet[2].replaceAll("[\\w]+:", "");
            users.add(new User(login,pass,role));
        }
        return users;
    }
}
