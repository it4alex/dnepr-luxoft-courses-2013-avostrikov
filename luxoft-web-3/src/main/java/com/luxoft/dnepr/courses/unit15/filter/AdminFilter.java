package com.luxoft.dnepr.courses.unit15.filter;

import com.luxoft.dnepr.courses.unit15.entity.User;
import com.luxoft.dnepr.courses.unit15.utils.Parser;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 11:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminFilter implements Filter {
    private Set<User> users;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String jsonUsers = filterConfig.getServletContext().getInitParameter("users");
        users = Parser.jsonToUsers(jsonUsers);
    }

    @Override
    public void doFilter(ServletRequest servletRequest
            , ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        // 1. ServletRequest/Response instead of Http... -> manual casting
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        if (!isAdmin((String) session.getAttribute("login"))) {
            response.sendRedirect("/myFirstWebApp/user");
            return;
        }
        filterChain.doFilter(request, response);
    }

    private boolean isAdmin(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login) &&
                    user.getRole().equals("admin")) return true;
        }
        return false;
    }

    @Override
    public void destroy() {
    }
}