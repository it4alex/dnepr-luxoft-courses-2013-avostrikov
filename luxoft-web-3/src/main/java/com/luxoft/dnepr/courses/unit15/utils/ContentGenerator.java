package com.luxoft.dnepr.courses.unit15.utils;

import com.luxoft.dnepr.courses.unit15.entity.SessionData;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 3:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class ContentGenerator {
    private ContentGenerator() {
    }

    public static String getWelcomePage(String user) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
                "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                "<head>\n" +
                "    <title>Login page</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
                "    <link type=\"text/css\" rel=\"stylesheet\" href=\"css/positions.css\"/>\n" +
                "    <link type=\"text/css\" rel=\"stylesheet\" href=\"css/theme_one.css\"/>\n" +
                "    <script type=\"text/javascript\" src=\"scripts/actions.js\"></script>" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"welcomeParent\">\n" +
                "    <div class=\"welcomeContent welcomeText\">Hello " + user + "!</div>\n" +
                "    <div class=\"welcomeRight welcomeLogout\"><a href=\"logout\">logout</a></div>\n" +
                "\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getIndexPage(boolean isError) {
        String errorMessage = "";
        if (isError) {
            errorMessage = "Wrong login or password";
        }
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
                "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                "<head>\n" +
                "    <title>Login page</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
                "    <link type=\"text/css\" rel=\"stylesheet\" href=\"css/positions.css\"/>\n" +
                "    <link type=\"text/css\" rel=\"stylesheet\" href=\"css/theme_one.css\"/>\n" +
                "    <script type=\"text/javascript\" src=\"scripts/actions.js\"></script>" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"parent\">\n" +
                "    <form action=\"index.html\" method=\"post\">\n" +
                "    <div class=\"leftCol\">\n" +
                "        <span class=\"text\">Login:</span><br />\n" +
                "        <span class=\"text\">Password: </span><br />\n" +
                "        <span class=\"text\"> </span>\n" +
                "    </div>\n" +
                "    <div class=\"centerCol\">\n" +
                "        <div class=\"login\"><input id=\"editInput\" class=\"editInput text\" type=\"text\" value=\"\" name=\"login\"/></div>\n" +
                "        <div class=\"pwd\"><input id=\"pwdInput\" class=\"editInput text\" type=\"password\" value=\"\" name=\"pwd\"/></div>\n" +
                "        <div class=\"button\"><input class=\"submit text\" id=\"submitButton\" type=\"submit\" title=\"\" value=\"Sign In\"/></div>\n" +
                "    </div>\n" +
                "        <div class=\"rightCol\"><span class=\"text errorMessage\">" + errorMessage + "</span></div>\n" +
                "    </form>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getStatistics(SessionData data)
    {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
                "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                "<head>\n" +
                "    <title>Statistics</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
                "    <link type=\"text/css\" rel=\"stylesheet\" href=\"../css/positions.css\"/>\n" +
                "    <link type=\"text/css\" rel=\"stylesheet\" href=\"../css/theme_one.css\"/>\n" +
                "    <script type=\"text/javascript\" src=\"../scripts/actions.js\"></script>" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"parent\">\n" +
                "<table>\n" +
                "    <tr class=\"trHeader\"><td>Parameter</td><td class=\"tdValue\">Value</td></tr>\n" +
                "    <tr class=\"trEvenRow\"><td>Active Sessions</td><td class=\"tdValue\">"+data.getActiveSessions()+"</td></tr>\n" +
                "    <tr><td>Active Sessions(ROLE user)</td><td class=\"tdValue\">"+data.getActiveSessionsUsers()+"</td></tr>\n" +
                "    <tr class=\"trEvenRow\"><td>Active Sessions(ROLE admin)</td><td class=\"tdValue\">"+data.getActiveSessionsAdmins()+"</td></tr>\n" +
                "    <tr><td>Total count of HttpRequests</td><td class=\"tdValue\">"+data.getTotalHttpRequests()+"</td></tr>\n" +
                "    <tr class=\"trEvenRow\"><td>Total count of POST HttpRequests</td><td class=\"tdValue\">"+data.getTotalPostHttpRequests()+"</td></tr>\n" +
                "    <tr><td>Total count of GET HttpRequests</td><td class=\"tdValue\">"+data.getTotalGetHttpRequests()+"</td></tr>\n" +
                "    <tr class=\"trEvenRow\"><td>Total count of Other HttpRequests</td><td class=\"tdValue\">"+data.getTotalOtherHttpRequests()+"</td></tr>\n" +
                "</table>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
    }
}
