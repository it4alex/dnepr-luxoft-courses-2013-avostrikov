use testdb;
/*task 1*/
CREATE TABLE IF NOT EXISTS makers (
  maker_id INT,
  maker_name VARCHAR(50) not null ,
  maker_address VARCHAR(200),
  CONSTRAINT pk_maker PRIMARY KEY(maker_id)
);

INSERT INTO makers (maker_id, maker_name, maker_address) values(1,'A','AdressA');
INSERT INTO makers (maker_id, maker_name, maker_address) values(2,'B','AdressB');
INSERT INTO makers (maker_id, maker_name, maker_address) values(3,'C','AdressC');
INSERT INTO makers (maker_id, maker_name, maker_address) values(4,'D','AdressD');
INSERT INTO makers (maker_id, maker_name, maker_address) values(5,'E','AdressE');


ALTER TABLE product
    add column maker_id int;

update product p, (SELECT * from makers) m
set p.maker_id = m.maker_id
where m.maker_name = p.maker;

ALTER TABLE product
  drop column maker,
  modify column maker_id int not null,
add constraint fk_product_makers FOREIGN KEY(maker_id) references makers(maker_id);

/*task 2*/

create table printer_type(
  type_id int ,
  type_name varchar(50) not null ,
  CONSTRAINT  pk_printer_type PRIMARY KEY (type_id)
);

insert into printer_type(type_id, type_name) values (1, 'Matrix');
insert into printer_type(type_id, type_name) values (2, 'Laser');
insert into printer_type(type_id, type_name) values (3, 'Jet');

ALTER TABLE printer
add column type_id int;

update printer p, (SELECT * from printer_type) t
set p.type_id = t.type_id
where t.type_name = p.type;

alter table printer
    drop type,
    modify column  type_id int not null,
  add constraint fk_printer_type FOREIGN KEY(type_id) references printer_type(type_id);

/*task 3*/

alter table printer
    modify column color char(1) not null default 'y';
/*task 4*/

CREATE INDEX ind_pc_price ON pc(price ASC) USING BTREE;
CREATE INDEX ind_laptop_price ON laptop(price ASC) USING BTREE;
CREATE INDEX ind_printer_price ON printer(price ASC) USING BTREE;

