use testdb;

DROP TABLE pc;
DROP TABLE laptop;
DROP TABLE printer;
DROP TABLE product;
DROP TABLE makers;
DROP TABLE printer_type;
