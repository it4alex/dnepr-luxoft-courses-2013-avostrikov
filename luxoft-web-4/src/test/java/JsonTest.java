import com.luxoft.dnepr.courses.unit16.entity.User;
import com.luxoft.dnepr.courses.unit16.utils.Parser;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/9/13
 * Time: 3:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class JsonTest {
    @Test
    public void checkJson() {
        String jsonValue =
                "{\n" +
                        "         {\"login\": \"user1\", \"password\": \"pswd1\", \"role\": \"user\"}\n" +
                        "         , {\"login\": \"user2\", \"password\": \"pswd2\", \"role\": \"user\"}\n" +
                        "         , {\"login\": \"user3\", \"password\": \"pswd3\", \"role\": \"admin\"}\n" +
                        "     }";
        Set<User> userSet = Parser.jsonToUsers(jsonValue);
        for(User user : userSet)
        {
            if(user.getLogin().equals("user3"))
                System.out.println(user.getRole());
        }

//        System.out.println(Arrays.toString(userSet.toArray()));

    }
}
