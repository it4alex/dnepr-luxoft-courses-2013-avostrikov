<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 6/20/13
  Time: 7:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Login page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="text/css" rel="stylesheet" href="./css/positions.css"/>
    <link type="text/css" rel="stylesheet" href="./css/theme_one.css"/>
    <script type="text/javascript" src="./scripts/actions.js"></script>
</head>
<body>
<div class="welcomeParent">                       <%--<%=request.getSession().getAttribute("login")%>--%>
    <div class="welcomeContent welcomeText">HELLO ${sessionScope.login}!</div>
    <div class="welcomeRight welcomeLogout"><a href="logout">logout</a></div>
</div>
</body>
</html>