<%@ page import="com.luxoft.dnepr.courses.unit16.entity.SessionData" %>
<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 6/20/13
  Time: 7:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Statistics</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="text/css" rel="stylesheet" href="../css/positions.css"/>
    <link type="text/css" rel="stylesheet" href="../css/theme_one.css"/>
    <script type="text/javascript" src="../scripts/actions.js"></script><link rel="stylesheet" type="text/css" href="../css/theme_one.css"/>
</head>
<body>
<% SessionData sd = (SessionData)request.getAttribute("sessionData");%>
<div class="parent">
    <table>
        <tr class="trHeader"><td>Parameter</td><td class="tdValue">Value</td></tr>
        <tr class="trEvenRow"><td>Active Sessions</td><td class="tdValue"><%=sd.getActiveSessions()%></td></tr>
        <tr><td>Active Sessions(ROLE user)</td><td class="tdValue"><%=sd.getActiveSessionsUsers()%></td></tr>
        <tr class="trEvenRow"><td>Active Sessions(ROLE admin)</td><td class="tdValue"><%=sd.getActiveSessionsAdmins()%></td></tr>
        <tr><td>Total count of HttpRequests</td><td class="tdValue"><%=sd.getTotalHttpRequests()%></td></tr>
        <tr class="trEvenRow"><td>Total count of POST HttpRequests</td><td class="tdValue"><%=sd.getTotalPostHttpRequests()%></td></tr>
        <tr><td>Total count of GET HttpRequests</td><td class="tdValue"><%=sd.getTotalGetHttpRequests()%></td></tr>
        <tr class="trEvenRow"><td>Total count of Other HttpRequests</td><td class="tdValue"><%=sd.getTotalOtherHttpRequests()%></td></tr>
    </table>
</div>
</body>
</html>
