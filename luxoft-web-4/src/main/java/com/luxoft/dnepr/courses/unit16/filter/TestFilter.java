package com.luxoft.dnepr.courses.unit16.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 11:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //FilterConfig == ServletConfig, just for Filters
    }

    @Override
    public void doFilter(ServletRequest servletRequest
            , ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        // 1. ServletRequest/Response instead of Http... -> manual casting
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // 2. Checking if user logged in
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute("login") != null) {
            response.sendRedirect("/login");
            return;
        }
        // 3. Logging users request
        request.getServletContext().log(String.format("User '%s' requests '$s'"
                , session.getAttribute("login"), request.getServletPath()));
        // 4. Transfer control over chain
        filterChain.doFilter(request, response);
        // 5. Setting encoding after returning from chain.
        // This might be done before doFilter() invocation.
        // But this is just an example.
        response.setCharacterEncoding("UTF-8");
    }

    @Override
    public void destroy() {
        // releasing of resources
    }
}