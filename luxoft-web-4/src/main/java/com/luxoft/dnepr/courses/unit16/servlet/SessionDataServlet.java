package com.luxoft.dnepr.courses.unit16.servlet;

import com.luxoft.dnepr.courses.unit16.entity.SessionData;
import com.luxoft.dnepr.courses.unit16.utils.Constants;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 3:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class SessionDataServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionData sessionData = new SessionData();

        sessionData.setActiveSessions(((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).get());
        sessionData.setActiveSessionsAdmins(((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE)).get());
        sessionData.setActiveSessionsUsers(((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE)).get());

        sessionData.setTotalHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_REQUESTS)).get());
        sessionData.setTotalGetHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_GET_REQUESTS)).get());
        sessionData.setTotalPostHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_POST_REQUESTS)).get());
        sessionData.setTotalOtherHttpRequests(((AtomicLong) getServletContext().getAttribute(Constants.HTTP_OTHER_REQUESTS)).get());

        request.setAttribute("sessionData",sessionData);
        RequestDispatcher view = request.getRequestDispatcher("/jsp/session_data.jsp");
        view.forward(request, response);
    }
}
