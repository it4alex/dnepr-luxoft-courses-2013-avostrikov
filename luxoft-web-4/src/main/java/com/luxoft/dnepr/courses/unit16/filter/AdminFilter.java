package com.luxoft.dnepr.courses.unit16.filter;

import com.luxoft.dnepr.courses.unit16.entity.User;
import com.luxoft.dnepr.courses.unit16.utils.Constants;
import com.luxoft.dnepr.courses.unit16.utils.Parser;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 11:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminFilter implements Filter {
    private Set<User> users;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        users = (Set<User>) filterConfig.getServletContext().getAttribute(Constants.SET_OF_USERS);
    }

    @Override
    public void doFilter(ServletRequest servletRequest
            , ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        if (session == null || !isAdmin((String)session.getAttribute("login"))) {
            response.sendRedirect("/myFirstWebApp/user");
            return;
        }
        filterChain.doFilter(request, response);
    }

    private boolean isAdmin(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login) &&
                    user.getRole().equals("admin")) return true;
        }
        return false;
    }

    @Override
    public void destroy() {
    }
}