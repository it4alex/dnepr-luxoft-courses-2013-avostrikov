package com.luxoft.dnepr.courses.unit16.servlet;

import com.luxoft.dnepr.courses.unit16.entity.User;
import com.luxoft.dnepr.courses.unit16.utils.Constants;
import com.luxoft.dnepr.courses.unit16.utils.Parser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 2:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        forwardWithMessage(request, response, "");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("pwd");
        User user = new User(login, password);

        if (isRegisteredUser(user)) {
            HttpSession session = request.getSession();
            session.setAttribute("login", request.getParameter("login"));
            session.setAttribute("role", getUsersRole(request.getParameter("login")));
            response.sendRedirect("/myFirstWebApp/user");
        } else {
            forwardWithMessage(request, response, "Wrong login or password");

        }
    }

    private boolean isRegisteredUser(User newUser) {
        Set<User> users = (Set<User>) getServletContext().getAttribute(Constants.SET_OF_USERS);
        return users.contains(newUser);
    }

    private String getUsersRole(String login) {
        Set<User> users = (Set<User>) getServletContext().getAttribute(Constants.SET_OF_USERS);
        for (User user : users) {
            if (user.getLogin().equals(login))
                return user.getRole();
        }
        return "";
    }

    private void forwardWithMessage(HttpServletRequest request, HttpServletResponse response,
                                    String message) throws ServletException, IOException {
        request.setAttribute("errorMessage", message);
        RequestDispatcher view = request.getRequestDispatcher("/jsp/login.jsp");
        view.forward(request, response);
    }
}
