package com.luxoft.dnepr.courses.unit16.listener;

import com.luxoft.dnepr.courses.unit16.entity.User;
import com.luxoft.dnepr.courses.unit16.utils.Constants;
import com.luxoft.dnepr.courses.unit16.utils.Parser;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE, new AtomicLong(0));

        sce.getServletContext().setAttribute(Constants.HTTP_REQUESTS, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_GET_REQUESTS, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_POST_REQUESTS, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_OTHER_REQUESTS, new AtomicLong(0));

        Set<User> users = Parser.jsonToUsers(sce.getServletContext().getInitParameter("users"));
        sce.getServletContext().setAttribute(Constants.SET_OF_USERS, users);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE);

        sce.getServletContext().removeAttribute(Constants.HTTP_REQUESTS);
        sce.getServletContext().removeAttribute(Constants.HTTP_GET_REQUESTS);
        sce.getServletContext().removeAttribute(Constants.HTTP_POST_REQUESTS);
        sce.getServletContext().removeAttribute(Constants.HTTP_OTHER_REQUESTS);

        sce.getServletContext().removeAttribute(Constants.SET_OF_USERS);
    }
}
