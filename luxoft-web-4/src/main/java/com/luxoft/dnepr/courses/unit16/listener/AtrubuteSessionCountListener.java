package com.luxoft.dnepr.courses.unit16.listener;

import com.luxoft.dnepr.courses.unit16.utils.Constants;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/17/13
 * Time: 1:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class AtrubuteSessionCountListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent sbe) {
        String role = (String)sbe.getSession().getAttribute("role");
        if(role.equals("admin"))
            getActiveAdminSessions(sbe).getAndIncrement();
        else if(role.equals("user"))
            getActiveUserSessions(sbe).getAndIncrement();
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent sbe) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent sbe) {

    }
    private AtomicLong getActiveAdminSessions(HttpSessionBindingEvent sbe) {
        return (AtomicLong) sbe.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE);
    }
    private AtomicLong getActiveUserSessions(HttpSessionBindingEvent sbe) {
        return (AtomicLong) sbe.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE);
    }
}
