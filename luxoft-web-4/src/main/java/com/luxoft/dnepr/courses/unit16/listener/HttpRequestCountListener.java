package com.luxoft.dnepr.courses.unit16.listener;

import com.luxoft.dnepr.courses.unit16.utils.Constants;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/17/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class HttpRequestCountListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {


    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        HttpServletRequest req = (HttpServletRequest) sre.getServletRequest();
        getHttpRequestCount(sre).getAndIncrement();
        if (req.getMethod().equals("GET")) {
            getHttpRequestGetCount(sre).getAndIncrement();

        } else if (req.getMethod().equals("POST")) {
            getHttpRequestPostCount(sre).getAndIncrement();

        } else {
            getHttpRequestOtherCount(sre).getAndIncrement();
        }
    }

    private AtomicLong getHttpRequestCount(ServletRequestEvent sre) {
        return (AtomicLong) ((HttpServletRequest) sre.getServletRequest()).
                getSession().getServletContext().getAttribute(Constants.HTTP_REQUESTS);
    }
    private AtomicLong getHttpRequestGetCount(ServletRequestEvent sre) {
        return (AtomicLong) ((HttpServletRequest) sre.getServletRequest()).
                getSession().getServletContext().getAttribute(Constants.HTTP_GET_REQUESTS);
    }
    private AtomicLong getHttpRequestPostCount(ServletRequestEvent sre) {
        return (AtomicLong) ((HttpServletRequest) sre.getServletRequest()).
                getSession().getServletContext().getAttribute(Constants.HTTP_POST_REQUESTS);
    }
    private AtomicLong getHttpRequestOtherCount(ServletRequestEvent sre) {
        return (AtomicLong) ((HttpServletRequest) sre.getServletRequest()).
                getSession().getServletContext().getAttribute(Constants.HTTP_OTHER_REQUESTS);
    }

}
