<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 6/20/13
  Time: 7:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
        session="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Login page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/positions.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/theme_one.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/actions.js"></script>
</head>
<body>
<div class="parent">
    <form action="${pageContext.request.contextPath}/index.html" method="post">
        <div class="leftCol">
            <span class="text">Login:</span><br />
            <span class="text">Password: </span><br />
            <span class="text"> </span>
        </div>
        <div class="centerCol">
            <div class="login"><input id="editInput" class="editInput text" type="text" value="" name="login"/></div>
            <div class="pwd"><input id="pwdInput" class="editInput text" type="password" value="" name="pwd"/></div>
            <div class="button"><input class="submit text" id="submitButton" type="submit" title="" value="Sign In"/></div>
        </div>
        <div class="rightCol"><span class="text errorMessage">${errorMessage}</span>
        </div>
    </form>
</div>
</body>
</html>