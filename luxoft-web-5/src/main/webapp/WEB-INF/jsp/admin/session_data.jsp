<%@ page import="com.luxoft.dnepr.courses.unit17.models.SessionData" %>
<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 6/20/13
  Time: 7:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
        session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Statistics</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/positions.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/theme_one.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/actions.js"></script>
</head>
<body>
<div class="parent">
    <table>
        <tr class="trHeader"><td>Parameter</td><td class="tdValue">Value</td></tr>
        <tr class="trEvenRow"><td>Active Sessions</td><td class="tdValue">${sessionData.activeSessions}</td></tr>
        <tr><td>Active Sessions(ROLE user)</td><td class="tdValue">${sessionData.activeSessionsUsers}</td></tr>
        <tr class="trEvenRow"><td>Active Sessions(ROLE admin)</td><td class="tdValue">${sessionData.activeSessionsAdmins}</td></tr>
        <tr><td>Total count of HttpRequests</td><td class="tdValue">${sessionData.totalHttpRequests}</td></tr>
        <tr class="trEvenRow"><td>Total count of POST HttpRequests</td><td class="tdValue">${sessionData.totalPostHttpRequests}</td></tr>
        <tr><td>Total count of GET HttpRequests</td><td class="tdValue">${sessionData.totalGetHttpRequests}</td></tr>
        <tr class="trEvenRow"><td>Total count of Other HttpRequests</td><td class="tdValue">${sessionData.totalOtherHttpRequests}</td></tr>
    </table>
</div>
</body>
</html>
