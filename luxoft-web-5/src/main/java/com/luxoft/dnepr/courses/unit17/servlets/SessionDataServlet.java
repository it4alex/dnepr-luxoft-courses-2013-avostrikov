package com.luxoft.dnepr.courses.unit17.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 3:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class SessionDataServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher(PATH_TO_SESSION_DATA_JSP);
        view.forward(request, response);
    }
}
