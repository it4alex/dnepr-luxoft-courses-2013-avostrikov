package com.luxoft.dnepr.courses.unit17.listeners;

import com.luxoft.dnepr.courses.unit17.models.SessionData;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/17/13
 * Time: 1:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class AttributeSessionCountListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent sbe) {
        String role = (String) sbe.getSession().getAttribute(ATTRIBUTE_ROLE);
        if (role.equals(ROLE_ADMIN))
            ((SessionData) sbe.getSession().getServletContext().
                    getAttribute(ATTRIBUTE_SESSION_DATA)).getActiveSessionsAdmins().getAndIncrement();
        else if (role.equals(ROLE_USER))
            ((SessionData) sbe.getSession().getServletContext().
                    getAttribute(ATTRIBUTE_SESSION_DATA)).getActiveSessionsUsers().getAndIncrement();
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent sbe) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent sbe) {
    }
}
