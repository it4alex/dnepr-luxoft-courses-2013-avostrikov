package com.luxoft.dnepr.courses.unit17.utils;

public interface Constants {
    String ATTRIBUTE_SET_OF_USERS = "ALLUSERS";

    String ATTRIBUTE_LOGIN = "login";
    String ATTRIBUTE_ROLE = "role";
    String ATTRIBUTE_PASSWORD = "pwd";
    String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    String ATTRIBUTE_SESSION_DATA = "sessionData";

    String PATH_TO_WELCOME_JSP = "/WEB-INF/jsp/welcome.jsp";
    String PATH_TO_LOGIN_JSP = "/WEB-INF/jsp/login.jsp";
    String PATH_TO_SESSION_DATA_JSP = "/WEB-INF/jsp/admin/session_data.jsp";
    String PATH_TO_INDEX_HTML = "/index.html";
    String PATH_TO_USER = "/user";

    String ROLE_USER = "user";
    String ROLE_ADMIN = "admin";

    String MESSAGE_ERROR_TO_LOGIN = "Wrong login or password";


}
