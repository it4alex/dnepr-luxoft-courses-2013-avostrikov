package com.luxoft.dnepr.courses.unit17.listeners;

import com.luxoft.dnepr.courses.unit17.models.SessionData;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/17/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class HttpRequestCountListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        HttpServletRequest req = (HttpServletRequest) sre.getServletRequest();

        ((SessionData) req.getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                getTotalHttpRequests().getAndIncrement();

        if (req.getMethod().equalsIgnoreCase("GET")) {
            ((SessionData) req.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                    getTotalGetHttpRequests().getAndIncrement();
        } else if (req.getMethod().equalsIgnoreCase("POST")) {
            ((SessionData) req.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                    getTotalPostHttpRequests().getAndIncrement();
        } else {
            ((SessionData) req.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                    getTotalOtherHttpRequests().getAndIncrement();
        }
    }
}
