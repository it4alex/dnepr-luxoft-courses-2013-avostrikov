package com.luxoft.dnepr.courses.unit17.filters;

import com.luxoft.dnepr.courses.unit17.models.User;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 11:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminFilter implements Filter {
    private Set<User> users;
    private String contextPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        users = (Set<User>) filterConfig.getServletContext().getAttribute(ATTRIBUTE_SET_OF_USERS);
        contextPath = filterConfig.getServletContext().getContextPath();
    }

    @Override
    public void doFilter(ServletRequest servletRequest
            , ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        if (session == null || !isAdmin((String) session.getAttribute(ATTRIBUTE_LOGIN))) {
            response.sendRedirect(contextPath + PATH_TO_USER);
            return;
        }
        filterChain.doFilter(request, response);
    }

    private boolean isAdmin(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login) &&
                    user.getRole().equals(ROLE_ADMIN)) return true;
        }
        return false;
    }

    @Override
    public void destroy() {
    }
}