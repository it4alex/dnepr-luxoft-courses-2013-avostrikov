package com.luxoft.dnepr.courses.unit17.listeners;

import com.luxoft.dnepr.courses.unit17.models.SessionData;
import com.luxoft.dnepr.courses.unit17.models.User;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

import com.luxoft.dnepr.courses.unit17.utils.Parser;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Set;

public class ApplicationContextListener implements ServletContextListener {
    private SessionData sessionData = new SessionData();

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        sce.getServletContext().setAttribute(ATTRIBUTE_SESSION_DATA, sessionData);

        Set<User> users = Parser.jsonToUsers(sce.getServletContext().getInitParameter("users"));
        sce.getServletContext().setAttribute(ATTRIBUTE_SET_OF_USERS, users);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(ATTRIBUTE_SESSION_DATA);
        sce.getServletContext().removeAttribute(ATTRIBUTE_SET_OF_USERS);
    }
}
