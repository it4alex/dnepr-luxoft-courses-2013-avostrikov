package com.luxoft.dnepr.courses.unit17.models;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with longelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 10:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class SessionData implements Serializable {
    private AtomicLong activeSessions;
    private AtomicLong activeSessionsUsers;
    private AtomicLong activeSessionsAdmins;
    private AtomicLong totalHttpRequests;
    private AtomicLong totalPostHttpRequests;
    private AtomicLong totalGetHttpRequests;
    private AtomicLong totalOtherHttpRequests;

    public SessionData() {
        activeSessions = new AtomicLong(0);
        activeSessionsUsers = new AtomicLong(0);
        activeSessionsAdmins = new AtomicLong(0);
        totalHttpRequests = new AtomicLong(0);
        totalPostHttpRequests = new AtomicLong(0);
        totalGetHttpRequests = new AtomicLong(0);
        totalOtherHttpRequests = new AtomicLong(0);
    }

    public AtomicLong getActiveSessions() {
        return activeSessions;
    }

    public void setActiveSessions(AtomicLong activeSessions) {
        this.activeSessions = activeSessions;
    }

    public AtomicLong getActiveSessionsUsers() {
        return activeSessionsUsers;
    }

    public void setActiveSessionsUsers(AtomicLong activeSessionsUsers) {
        this.activeSessionsUsers = activeSessionsUsers;
    }

    public AtomicLong getActiveSessionsAdmins() {
        return activeSessionsAdmins;
    }

    public void setActiveSessionsAdmins(AtomicLong activeSessionsAdmins) {
        this.activeSessionsAdmins = activeSessionsAdmins;
    }

    public AtomicLong getTotalHttpRequests() {
        return totalHttpRequests;
    }

    public void setTotalHttpRequests(AtomicLong totalHttpRequests) {
        this.totalHttpRequests = totalHttpRequests;
    }

    public AtomicLong getTotalPostHttpRequests() {
        return totalPostHttpRequests;
    }

    public void setTotalPostHttpRequests(AtomicLong totalPostHttpRequests) {
        this.totalPostHttpRequests = totalPostHttpRequests;
    }

    public AtomicLong getTotalGetHttpRequests() {
        return totalGetHttpRequests;
    }

    public void setTotalGetHttpRequests(AtomicLong totalGetHttpRequests) {
        this.totalGetHttpRequests = totalGetHttpRequests;
    }

    public AtomicLong getTotalOtherHttpRequests() {
        return totalOtherHttpRequests;
    }

    public void setTotalOtherHttpRequests(AtomicLong totalOtherHttpRequests) {
        this.totalOtherHttpRequests = totalOtherHttpRequests;
    }
}