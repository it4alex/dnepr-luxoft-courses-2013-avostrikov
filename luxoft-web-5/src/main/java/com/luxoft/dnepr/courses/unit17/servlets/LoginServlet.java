package com.luxoft.dnepr.courses.unit17.servlets;

import com.luxoft.dnepr.courses.unit17.models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 2:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        forwardWithMessage(request, response, "");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter(ATTRIBUTE_LOGIN);
        String password = request.getParameter(ATTRIBUTE_PASSWORD);
        User user = new User(login, password);

        if (isRegisteredUser(user)) {
            HttpSession session = request.getSession();
            session.setAttribute(ATTRIBUTE_LOGIN, request.getParameter(ATTRIBUTE_LOGIN));
            session.setAttribute(ATTRIBUTE_ROLE, getUsersRole(request.getParameter(ATTRIBUTE_LOGIN)));

            response.sendRedirect(getServletContext().getContextPath() + PATH_TO_USER);
        } else {
            forwardWithMessage(request, response, MESSAGE_ERROR_TO_LOGIN);

        }
    }

    private boolean isRegisteredUser(User newUser) {
        Set<User> users = (Set<User>) getServletContext().getAttribute(ATTRIBUTE_SET_OF_USERS);
        return users.contains(newUser);
    }

    private String getUsersRole(String login) {
        Set<User> users = (Set<User>) getServletContext().getAttribute(ATTRIBUTE_SET_OF_USERS);
        for (User user : users) {
            if (user.getLogin().equals(login))
                return user.getRole();
        }
        return "";
    }

    private void forwardWithMessage(HttpServletRequest request, HttpServletResponse response,
                                    String message) throws ServletException, IOException {
        request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, message);
        RequestDispatcher view = request.getRequestDispatcher(PATH_TO_LOGIN_JSP);
        view.forward(request, response);
    }
}
