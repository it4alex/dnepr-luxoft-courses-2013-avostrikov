package com.luxoft.dnepr.courses.unit17.listeners;

import com.luxoft.dnepr.courses.unit17.models.SessionData;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class ActiveSessionCountHttpSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        ((SessionData) hse.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                getActiveSessions().getAndIncrement();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        ((SessionData) hse.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                getActiveSessions().getAndDecrement();

        String role = (String) hse.getSession().getAttribute(ATTRIBUTE_ROLE);
        if (role.equals(ROLE_ADMIN))
            ((SessionData) hse.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                    getActiveSessionsAdmins().getAndDecrement();
        else if (role.equals(ROLE_USER))
            ((SessionData) hse.getSession().getServletContext().getAttribute(ATTRIBUTE_SESSION_DATA)).
                    getActiveSessionsUsers().getAndDecrement();
    }

}
