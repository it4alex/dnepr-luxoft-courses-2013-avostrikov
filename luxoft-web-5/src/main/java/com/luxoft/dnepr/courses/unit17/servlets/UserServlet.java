package com.luxoft.dnepr.courses.unit17.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.luxoft.dnepr.courses.unit17.utils.Constants.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 6/16/13
 * Time: 3:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session != null) {
            RequestDispatcher view =
                    request.getRequestDispatcher(PATH_TO_WELCOME_JSP);
            view.forward(request, response);
        } else response.sendRedirect(getServletContext().getContextPath() + PATH_TO_INDEX_HTML);


    }
}
