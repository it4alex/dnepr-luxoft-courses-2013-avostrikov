package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class _BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();

        assertNotSame(book, cloned);

        assertEquals(book.getCode(), cloned.getCode());
        assertEquals(book.getName(), cloned.getName());
        assertEquals(book.getPrice(), cloned.getPrice(), 0);
        assertEquals(book.getPublicationDate(), cloned.getPublicationDate());

        assertNotSame(book.getPublicationDate(), cloned.getPublicationDate());

        //check that changing of date on cloned object doesn't affect original
        cloned.getPublicationDate().setTime(0);
        assertNotEquals(book.getPublicationDate(), cloned.getPublicationDate());
    }

    @Test
    public void testEquals() {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book sameBook = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book oneMoreSameBook = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());

        //it should be reflexive
        assertEquals(book, book);

        //it should be symmetric
        assertEquals(book, sameBook);
        assertEquals(sameBook, book);

        //it should be transitive
        assertEquals(book, sameBook);
        assertEquals(sameBook, oneMoreSameBook);
        assertEquals(book, oneMoreSameBook);

        //equals(null) should return false
        assertFalse(book.equals(null));

        Book bookWithDiffPrice = productFactory.createBook("code", "Thinking in Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        Book bookWithDiffCode = productFactory.createBook("code1", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book bookWithDiffName = productFactory.createBook("code1", "Unknown name", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book bookWithDiffPubDate = productFactory.createBook("code1", "Unknown name", 200, new GregorianCalendar(2000, 0, 1).getTime());

        assertEquals(book, bookWithDiffPrice);
        assertNotEquals(book, bookWithDiffCode);
        assertNotEquals(book, bookWithDiffName);
        assertNotEquals(book, bookWithDiffPubDate);

        //check that book is not equal to bread
        assertNotEquals(book, productFactory.createBread("code", "Thinking in Java", 200, 0));
    }

    @Test
    public void testEqualsWithNullValues() {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());

        //check correct handling of null values
        assertNotEquals(book, productFactory.createBook(null, null, 0, null));
        assertNotEquals(productFactory.createBook(null, null, 0, null), book);
        assertEquals(productFactory.createBook(null, null, 0, null), productFactory.createBook(null, null, 0, null));
    }

    @Test
    public void testEqualsStringComparison() {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());

        //this is trick to check whether strings are compared by == instead of equals()
        String codeNewInstance = new String("code");
        Book bookWithCalculatedCode =
                productFactory.createBook(codeNewInstance, "Thinking in Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        assertEquals("Incorrect string comparision: use equals() instead of == ", book, bookWithCalculatedCode);
    }

    @Test
    public void testHashCode() {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book sameBook = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book bookWithDiffPrice = productFactory.createBook("code", "Thinking in Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        Book differentBook = productFactory.createBook("diff", "C#", 200, new GregorianCalendar(2005, 0, 1).getTime());

        assertEquals(book.hashCode(), sameBook.hashCode());
        assertEquals(book.hashCode(), bookWithDiffPrice.hashCode());
        assertNotEquals(book.hashCode(), differentBook.hashCode());
    }
}
