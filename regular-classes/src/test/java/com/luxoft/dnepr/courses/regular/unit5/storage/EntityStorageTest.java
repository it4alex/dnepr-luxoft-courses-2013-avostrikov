package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.dao.AbstractDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/9/13
 * Time: 7:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorageTest {
    private IDao<Employee> employeeDao;
    private IDao<Redis> redisDao;
    private IDao<Redis> abstractRedisDao;

    @Before
    public void setUp() throws Exception {
        employeeDao = new EmployeeDaoImpl();
        redisDao = new RedisDaoImpl();
        abstractRedisDao = new AbstractDaoImpl<>();


    }

    @After
    public void tearDown() throws Exception {
        //remove all entities from Singleton
        Map<Long, Entity> entities = EntityStorage.getEntities();
        entities.clear();
        employeeDao = null;
        redisDao = null;
        abstractRedisDao = null;

    }

    @Test
    public void testRedisNewId() throws Exception {
        Redis redisWithNullId = new Redis(null, 10);
        Assert.assertTrue(redisDao.save(redisWithNullId).getId().equals(1L));
        redisWithNullId = new Redis(null, 10);
        Assert.assertTrue(redisDao.save(redisWithNullId).getId().equals(2L));
        Redis redisWithId = new Redis(1000L, 1000);
        Assert.assertTrue(redisDao.save(redisWithId).getId().equals(1000L));
        redisWithNullId = new Redis(null, 10);
        Assert.assertTrue(redisDao.save(redisWithNullId).getId().equals(1001L));
    }

    @Test
    public void testEmployeeNewId() throws Exception {
        Employee employeeWithNullId = new Employee(null, 10);
        Assert.assertTrue(employeeDao.save(employeeWithNullId).getId().equals(1L));
        employeeWithNullId = new Employee(null, 10);
        Assert.assertTrue(employeeDao.save(employeeWithNullId).getId().equals(2L));
        Employee redisWithId = new Employee(1000L, 1000);
        Assert.assertTrue(employeeDao.save(redisWithId).getId().equals(1000L));
        employeeWithNullId = new Employee(null, 10);
        Assert.assertTrue(employeeDao.save(employeeWithNullId).getId().equals(1001L));

    }

    @Test
    public void testEntities() throws Exception {
        Employee employee = new Employee(1L, 500);
        Redis redis = new Redis(2L, 1000);

        redisDao.save(redis);
        employeeDao.save(employee);
        Assert.assertTrue(EntityStorage.getEntities().get(1L) instanceof Employee);
        Assert.assertTrue(EntityStorage.getEntities().get(2L) instanceof Redis);

    }
    @Test
    public void testEntitiesAbstract() throws Exception {
        Redis redis = new Redis(2L, 1000);
        abstractRedisDao.save(redis);
        Assert.assertTrue(EntityStorage.getEntities().get(2L) instanceof Redis);

    }

    @Test(expected = UserAlreadyExist.class)
    public void testEmployeeAlreadyExistException() throws Exception {
        Employee employee1 = new Employee(1L, 1000);
        Employee employee2 = new Employee(1L, 1000);
        employeeDao.save(employee1);
        employeeDao.save(employee2);

    }

    @Test(expected = UserAlreadyExist.class)
    public void testRedisAlreadyExistException() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        Redis redis2 = new Redis(2L, 2000);
        redisDao.save(redis1);
        redisDao.save(redis2);

    }

    @Test(expected = UserNotFound.class)
    public void testEmployeeUpdateWrongId() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        Employee employee2 = new Employee(1000L, 1000);
        employeeDao.save(employee1);
        employeeDao.update(employee2);
    }

    @Test(expected = UserNotFound.class)
    public void testEmployeeUpdateNullId() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        Employee employee2 = new Employee(null, 1000);
        employeeDao.save(employee1);
        employeeDao.update(employee2);
    }

    @Test(expected = UserNotFound.class)
    public void testEmployeeUpdateNullEmployee() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        employeeDao.save(employee1);
        employeeDao.update(null);
    }

    @Test(expected = UserNotFound.class)
    public void testRedisUpdateWrongId() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        Redis redis2 = new Redis(2000L, 2000);
        redisDao.save(redis1);
        redisDao.update(redis2);

    }

    @Test(expected = UserNotFound.class)
    public void testRedisUpdateNullId() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        Redis redis2 = new Redis(null, 2000);
        redisDao.save(redis1);
        redisDao.update(redis2);
    }

    @Test(expected = UserNotFound.class)
    public void testRedisUpdateNullRedis() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        redisDao.save(redis1);
        redisDao.update(null);
    }

    @Test
    public void testRedisGet() throws Exception {
        Redis redis1 = new Redis(1L, 1000);
        redisDao.save(redis1);
        Assert.assertTrue(redisDao.get(1000L) == null);
    }

    @Test
    public void testEmployeeGet() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        employeeDao.save(employee1);
        Assert.assertTrue(employeeDao.get(1000L) == null);
    }

    @Test
    public void testRedisDelete() throws Exception {
        Redis redis1 = new Redis(1L, 1000);
        Redis redis2 = new Redis(2L, 1000);
        Redis redis3 = new Redis(3L, 1000);
        Redis redis4 = new Redis(4L, 1000);
        redisDao.save(redis1);
        redisDao.save(redis2);
        redisDao.save(redis3);
        redisDao.save(redis4);


        Assert.assertTrue(redisDao.delete(1L));
        Assert.assertFalse(redisDao.delete(100L));
        Assert.assertFalse(redisDao.delete(100L));
        Assert.assertTrue(redisDao.delete(3L));
        Assert.assertTrue(redisDao.delete(2L));
        Assert.assertTrue(redisDao.delete(4L));
        Assert.assertFalse(redisDao.delete(4L));
    }

    @Test
    public void testEmployeeDelete() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        Employee employee2 = new Employee(2L, 500);
        Employee employee3 = new Employee(3L, 500);
        Employee employee4 = new Employee(4L, 500);
        employeeDao.save(employee1);
        employeeDao.save(employee2);
        employeeDao.save(employee3);
        employeeDao.save(employee4);

        Assert.assertTrue(employeeDao.delete(1L));
        Assert.assertFalse(employeeDao.delete(200L));
        Assert.assertTrue(employeeDao.delete(2L));
        Assert.assertTrue(employeeDao.delete(3L));
        Assert.assertTrue(employeeDao.delete(4L));
        Assert.assertFalse(employeeDao.delete(4L));
    }

}
