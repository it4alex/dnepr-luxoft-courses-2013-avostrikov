package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

public class _ThreadProducerBlockedThreadTest {

    @Test
    public void testGetBlockedThread() throws Exception {
        Assert.assertEquals(Thread.State.BLOCKED, ThreadProducer.getBlockedThread().getState());
    }

}
