package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

public class _ThreadProducerRunnableThreadTest {

    @Test
    public void testGetRunnableThread() throws Exception {
        Thread thread = ThreadProducer.getRunnableThread();
        Assert.assertEquals(Thread.State.RUNNABLE, thread.getState());
    }

}
