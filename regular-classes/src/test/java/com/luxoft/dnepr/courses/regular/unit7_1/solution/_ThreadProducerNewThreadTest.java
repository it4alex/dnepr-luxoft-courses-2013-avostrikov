package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

public class _ThreadProducerNewThreadTest {

    @Test
    public void testGetNewThread() throws Exception {
        Assert.assertEquals(Thread.State.NEW, ThreadProducer.getNewThread().getState());
    }

}
