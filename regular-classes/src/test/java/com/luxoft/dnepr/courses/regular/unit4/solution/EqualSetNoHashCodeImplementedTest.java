package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class EqualSetNoHashCodeImplementedTest {

    private Set<NoHashCode> set = new EqualSet<NoHashCode>();

    @Before
    public void init() {
        set.add(new NoHashCode(1, "a", 1));
        set.add(new NoHashCode(2, "b", 2));
        set.add(new NoHashCode(3, "c", 3));
        set.add(new NoHashCode(4, "d", 4));
        set.add(new NoHashCode(5, "e", 5));
    }

    @Test
    public void testSize() throws Exception {
        Assert.assertEquals(5, set.size());
    }

    @Test
    public void testIsEmpty() throws Exception {
        Assert.assertFalse(set.isEmpty());
        set.remove(new NoHashCode(1, "a", 1));
        Assert.assertFalse(set.isEmpty());
        set.remove(new NoHashCode(2, "b", 2));
        Assert.assertFalse(set.isEmpty());
        set.remove(new NoHashCode(3, "c", 3));
        Assert.assertFalse(set.isEmpty());
        set.remove(new NoHashCode(4, "d", 4));
        Assert.assertFalse(set.isEmpty());
        set.remove(new NoHashCode(5, "e", 5));
        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        Assert.assertTrue(set.contains(new NoHashCode(1, "a", 1)));
        Assert.assertTrue(set.contains(new NoHashCode(3, "c", 3)));
        Assert.assertTrue(set.contains(new NoHashCode(5, "e", 5)));
        Assert.assertFalse(set.contains(new NoHashCode(6, "e", 5)));
        Assert.assertFalse(set.contains(new NoHashCode(7, "e", 5)));
        Assert.assertFalse(set.contains(11));
    }

    @Test
    public void testIterator() throws Exception {
        Iterator<NoHashCode> iterator = set.iterator();
        List<NoHashCode> actual = new ArrayList<NoHashCode>();
        while (iterator.hasNext()) {
            NoHashCode i = iterator.next();
            actual.add(i);
        }
        List<NoHashCode> expected = new ArrayList<NoHashCode>();
        expected.add(new NoHashCode(1, "a", 1));
        expected.add(new NoHashCode(2, "b", 2));
        expected.add(new NoHashCode(3, "c", 3));
        expected.add(new NoHashCode(4, "d", 4));
        expected.add(new NoHashCode(5, "e", 5));
        Assert.assertEquals(expected.size(), actual.size());
        Assert.assertTrue(expected.containsAll(actual) && actual.containsAll(expected));
    }

    @Test
    public void testToArray() throws Exception {
        Object[] actual = set.toArray();
        Assert.assertEquals(5, actual.length);
        List<Object> actualList = Arrays.asList(actual);
        Assert.assertTrue(actualList.contains(new NoHashCode(1, "a", 1)));
        Assert.assertTrue(actualList.contains(new NoHashCode(2, "b", 2)));
        Assert.assertTrue(actualList.contains(new NoHashCode(3, "c", 3)));
        Assert.assertTrue(actualList.contains(new NoHashCode(4, "d", 4)));
        Assert.assertTrue(actualList.contains(new NoHashCode(5, "e", 5)));

    }

    @Test
    public void testToArray2() throws Exception {
        NoHashCode[] actual = set.toArray(new NoHashCode[]{});
        Assert.assertEquals(5, actual.length);
        List<NoHashCode> actualList = Arrays.asList(actual);
        Assert.assertTrue(actualList.contains(new NoHashCode(1, "a", 1)));
        Assert.assertTrue(actualList.contains(new NoHashCode(2, "b", 2)));
        Assert.assertTrue(actualList.contains(new NoHashCode(3, "c", 3)));
        Assert.assertTrue(actualList.contains(new NoHashCode(4, "d", 4)));
        Assert.assertTrue(actualList.contains(new NoHashCode(5, "e", 5)));
    }

    @Test
    public void testAdd() throws Exception {
        Assert.assertTrue(set.add(new NoHashCode(1, "a", 11)));
        Assert.assertTrue(set.add(new NoHashCode(1, "a", 12)));
        Assert.assertTrue(set.add(new NoHashCode(1, "a", 13)));
        Assert.assertTrue(set.add(new NoHashCode(1, "a", 14)));
        Assert.assertTrue(set.add(new NoHashCode(1, "a", 15)));

        Assert.assertFalse(set.add(new NoHashCode(1, "a", 1)));
        Assert.assertFalse(set.add(new NoHashCode(2, "b", 2)));
        Assert.assertFalse(set.add(new NoHashCode(1, "a", 13)));
        Assert.assertFalse(set.add(new NoHashCode(1, "a", 14)));
        Assert.assertFalse(set.add(new NoHashCode(1, "a", 15)));

        Assert.assertEquals(10, set.size());
    }

    @Test
    public void testAddNullValue() throws Exception {
        Assert.assertTrue(set.add(null));
        Assert.assertEquals(6, set.size());
        Assert.assertFalse(set.add(null));
        Assert.assertEquals(6, set.size());
        Assert.assertFalse(set.add(null));
        Assert.assertEquals(6, set.size());
    }

    @Test
    public void testRemove() throws Exception {
        Assert.assertTrue(set.remove(new NoHashCode(1, "a", 1)));
        Assert.assertTrue(set.remove(new NoHashCode(2, "b", 2)));
        Assert.assertFalse(set.remove(new NoHashCode(1, "a", 11)));
        Assert.assertFalse(set.remove(new NoHashCode(1, "a", 12)));
        Assert.assertFalse(set.remove(new NoHashCode(1, "a", 13)));

        Assert.assertEquals(3, set.size());
    }

    @Test
    public void testRemoveNull() throws Exception {
        set.add(null);
        Assert.assertTrue(set.remove(null));
        Assert.assertFalse(set.remove(null));
        Assert.assertFalse(set.remove(null));
    }

    @Test
    public void testClear() throws Exception {
        set.clear();
        Assert.assertTrue(set.isEmpty());
    }

    public static void assertSetEquals(Set setExpected, Set setActual) {
        Assert.assertEquals(setExpected.size(), setActual.size());
        Object[] arrayExpected = new Object[setExpected.size()];
        Object[] arrayActual = new Object[setActual.size()];
        int i = 0;
        for (Object o : setExpected) {
            arrayExpected[i] = o;
        }
        for (Object o : setActual) {
            arrayActual[i] = o;
        }
        Assert.assertArrayEquals(arrayExpected, arrayActual);
    }

    public static class NoHashCode {
        int field1;
        String field2;
        long field3;

        NoHashCode(int field1, String field2, long field3) {
            this.field1 = field1;
            this.field2 = field2;
            this.field3 = field3;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o instanceof NoHashCode) {
                NoHashCode nfc = (NoHashCode) o;
                return field1 == nfc.field1
                        && field3 == nfc.field3
                        && (field2 == nfc.field2 || field2 != null && field2.equals(nfc.field2));
            }
            return false;
        }

    }
}
