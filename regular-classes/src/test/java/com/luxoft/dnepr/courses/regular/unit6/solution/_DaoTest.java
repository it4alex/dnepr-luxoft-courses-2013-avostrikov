package com.luxoft.dnepr.courses.regular.unit6;

import com.luxoft.dnepr.courses.regular.unit6.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit6.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.model.Redis;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class _DaoTest {

    private static int ACTIVE_THREADS = 100;
    IDao<Redis> redisDao = new RedisDaoImpl();
    IDao<Employee> employeeDao = new EmployeeDaoImpl();

    @Before
    public void init() {
        EntityStorage.getEntities().clear();
        EntityStorage.getEntities().put(1L, createRedis(1L, 1));
        EntityStorage.getEntities().put(2L, createRedis(2L, 2));
        EntityStorage.getEntities().put(3L, createRedis(3L, 3));
        EntityStorage.getEntities().put(4L, createRedis(4L, 4));
        EntityStorage.getEntities().put(5L, createRedis(5L, 5));
        EntityStorage.getEntities().put(11L, createEmployee(11L, 1));
        EntityStorage.getEntities().put(12L, createEmployee(12L, 2));
        EntityStorage.getEntities().put(13L, createEmployee(13L, 3));
        EntityStorage.getEntities().put(14L, createEmployee(14L, 4));
        EntityStorage.getEntities().put(15L, createEmployee(15L, 5));
    }

    @Test
    public void saveTest() throws InterruptedException {
        class EmployeeThread extends Thread {
            public void run() {
                Employee employee = new Employee();
                employee.setSalary(10);
                employeeDao.save(employee);
            }
        }
        class RedisThread extends Thread {
            public void run() {
                Redis redis = new Redis();
                redis.setWeight(20);
                redisDao.save(redis);
            }
        }
        Thread[] creatorThreads = new Thread[ACTIVE_THREADS];
        for (int i = 0; i < creatorThreads.length; i++) {
            creatorThreads[i] = i % 2 == 0 ? new EmployeeThread() : new RedisThread();
        }
        for (Thread thread : creatorThreads) {
            thread.start();
        }
        for (Thread thread : creatorThreads) {
            thread.join();
        }
        Assert.assertEquals(10 + ACTIVE_THREADS, EntityStorage.getEntities().size());
    }

    private static Redis createRedis(Long id, int weight) {
        Redis redis = new Redis();
        redis.setId(id);
        redis.setWeight(weight);
        return redis;
    }

    private static Employee createEmployee(Long id, int salary) {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setSalary(salary);
        return employee;
    }
}
