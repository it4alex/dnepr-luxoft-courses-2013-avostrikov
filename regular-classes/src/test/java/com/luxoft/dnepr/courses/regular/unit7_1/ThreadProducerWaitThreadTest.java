package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/16/13
 * Time: 2:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ThreadProducerWaitThreadTest {
    @Test
    public void getWaitThreadTest() {
        Thread thread = ThreadProducer.getWaitingThread();

        Assert.assertTrue(thread.getState() == Thread.State.WAITING);
        ThreadProducer.printState(thread);
        thread.interrupt();
    }

    @Test
    public void getTimedWaitThreadTest() {
        Thread thread = ThreadProducer.getTimedWaitingThread();
        Assert.assertTrue(thread.getState() == Thread.State.TIMED_WAITING);
        ThreadProducer.printState(thread);
        thread.interrupt();
    }

}
