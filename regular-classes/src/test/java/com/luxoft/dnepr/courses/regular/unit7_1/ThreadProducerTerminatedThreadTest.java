package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/16/13
 * Time: 2:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ThreadProducerTerminatedThreadTest {
    @Test
    public void getTerminatedThreadTest() {
        Thread thread = ThreadProducer.getTerminatedThread();
        Assert.assertTrue(thread.getState() == Thread.State.TERMINATED);
        ThreadProducer.printState(thread);
    }

}
