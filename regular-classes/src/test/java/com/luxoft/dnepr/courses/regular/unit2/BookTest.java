package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.assertTrue;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200.34, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        Assert.assertEquals(book.getCode(), cloned.getCode());
        Assert.assertEquals(book.getName(), cloned.getName());
        Assert.assertEquals(book.getPrice(), cloned.getPrice(), 0.001);
        Assert.assertTrue(book.getPublicationDate().toString().equals(cloned.getPublicationDate().toString()));
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200.34, new GregorianCalendar(2006, 0, 1).getTime());
        Book equalBook = productFactory.createBook("code", "Thinking in Java", 200.34, new GregorianCalendar(2006, 0, 1).getTime());
        Book equalBookExceptPrice = productFactory.createBook("code", "Thinking in Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        Book notEqualBook = productFactory.createBook("code2", "Java Beans", 200.34, new GregorianCalendar(2006, 0, 1).getTime());

        assertTrue(book.equals(equalBook));
        Assert.assertTrue(equalBook.equals(book));
        Assert.assertTrue(book.equals(equalBookExceptPrice));
        Assert.assertFalse(book.equals(notEqualBook));

    }

}
