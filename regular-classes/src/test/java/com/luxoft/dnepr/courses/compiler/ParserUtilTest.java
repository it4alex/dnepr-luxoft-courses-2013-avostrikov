package com.luxoft.dnepr.courses.compiler;

import junit.framework.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/17/13
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParserUtilTest {
    @Test
    public void testReplaceAllByPattern() {
        String source = "(  6+2 )*5-    8/4";
        String result = ParserUtil.replaceAllByPattern(source, "[\\s,]+", "");
        Assert.assertEquals("(6+2)*5-8/4", result);
    }

    @Test
    public void testRemoveSpaces() {
        String source = "(  6+2 )*5-    8/4";
        String result = ParserUtil.removeSpaces(source);
        Assert.assertEquals("(6+2)*5-8/4", result);
    }


    @Test
    public void testAddSpaces() {
        String source = "(6+2)*5-8/4";
        String result = ParserUtil.addSpaces(source);
        Assert.assertEquals("( 6 + 2 ) * 5 - 8 / 4", result);
    }

}

