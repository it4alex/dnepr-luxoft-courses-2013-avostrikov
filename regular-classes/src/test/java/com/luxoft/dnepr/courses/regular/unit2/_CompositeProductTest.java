/***********************************************************************************
 *
 * Copyright 2013 - 2013 Yota Lab LLC, Russia
 * Copyright 2013 - 2013 Seconca Holdings Limited, Cyprus
 *
 *  This source code is Yota Lab Confidential Proprietary
 *  This software is protected by copyright.  All rights and titles are reserved.
 *  You shall not use, copy, distribute, modify, decompile, disassemble or reverse
 *  engineer the software. Otherwise this violation would be treated by law and
 *  would be subject to legal prosecution.  Legal use of the software provides
 *  receipt of a license from the right holder only.
 *
 *
 ************************************************************************************/

package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class _CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());
    }

    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();
        assertEquals(0, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10)* 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20 + 20) * 0.9, compositeProduct.getPrice(), 0);
    }
}
