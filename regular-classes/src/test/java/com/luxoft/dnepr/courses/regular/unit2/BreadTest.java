package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class BreadTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Bread bread = productFactory.createBread("br", "Bread", 10, 1.0);
        Bread cloned = (Bread) bread.clone();
        Assert.assertEquals(bread.getCode(), cloned.getCode());
        Assert.assertEquals(bread.getName(), cloned.getName());
        Assert.assertEquals(bread.getPrice(), cloned.getPrice(), 0.001);
        Assert.assertEquals(bread.getWeight(), cloned.getWeight(), 0.001);
    }

    @Test
    public void testEquals() throws Exception {
        Bread bread = productFactory.createBread("br", "Bread", 10, 1.0);
        Bread equalBread = productFactory.createBread("br", "Bread", 10, 1.0);
        Bread equalBreadExceptPrice = productFactory.createBread("br", "Bread", 100, 1.0);
        Bread notEqualBread = productFactory.createBread("br2", "Bread2", 10, 1.0);

        assertTrue(bread.equals(equalBread));
        Assert.assertTrue(equalBread.equals(bread));
        Assert.assertTrue(bread.equals(equalBreadExceptPrice));
        Assert.assertFalse(bread.equals(notEqualBread));

    }

}
