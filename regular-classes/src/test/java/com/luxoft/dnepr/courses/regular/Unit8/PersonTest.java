package com.luxoft.dnepr.courses.regular.Unit8;

import com.luxoft.dnepr.courses.regular.unit8.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit8.Gender;
import com.luxoft.dnepr.courses.regular.unit8.Person;
import com.luxoft.dnepr.courses.regular.unit8.Serializer;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/22/13
 * Time: 9:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonTest {

    @Test
    public void personToJsonTest() throws IOException, ClassNotFoundException {

        Person father = new Person("****UserFather", Gender.MALE, "ukrainian", new Date(), null, null);
        Person mother = new Person("****UserMother", Gender.FEMALE, "ukrainian", new Date(), null, null);
        Person son = new Person("****UserSon", Gender.MALE, "ukrainian", new Date(), father,mother);



//        Serializer.serialize(new File("sds"), new FamilyTree(son));
        FileOutputStream fos = new FileOutputStream("testfile.txt");
        OutputStream con = System.out;
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(son);
        oos.flush();
        oos.close();
//



        son = null;
        FileInputStream fis = new FileInputStream("testfile.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        son = (Person)ois.readObject();
        ois.close();



//        ObjectOutputStream oosc = new ObjectOutputStream(con);
//
//        oosc.writeObject(son);
//        oosc.flush();
//        oosc.close();


//        System.out.println("username: " + userRead.getUsername());
//
//
//        Person person = new Person();
//        person.setBirthDate(new Date());
//        person.setEthnicity("ukrainian");
//        person.setGender(Gender.FEMALE);
//        person.setName("Galja");
//
////        String jsonExpected = "{\"name\":\"Galja\",\"gender\":\"FEMALE\",\"ethnicity\":\"ukrainian\",\"birthDate\":\"May 21, 2013 3:31:33 PM\",\"father\":{\"name\":\"Vasiliy\",\"gender\":\"MALE\",\"ethnicity\":\"ukrainian\",\"birthDate\":\"May 21, 2013 3:31:33 PM\"}}";
//        String jsonExpected = "{\"name\":\"Galja\",\"gender\":\"FEMALE\",\"ethnicity\":\"ukrainian\",\"birthDate\":\"May 21, 2013 3:31:33 PM\",\"father\":{\"name\":\"Vasiliy\",\"gender\":\"MALE\",\"ethnicity\":\"ukrainian\",\"birthDate\":\"May 21, 2013 3:31:33 PM\"}}";
////        junit.framework.Assert.assertTrue(obj.toJson().equals(jsonExpected));
    }
}
