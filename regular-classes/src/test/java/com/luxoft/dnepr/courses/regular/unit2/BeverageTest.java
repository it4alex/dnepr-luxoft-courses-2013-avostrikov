package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class BeverageTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Beverage Beverage = productFactory.createBeverage("bev", "Cola", 10, true);
        Beverage cloned = (Beverage) Beverage.clone();
        Assert.assertEquals(Beverage.getCode(), cloned.getCode());
        Assert.assertEquals(Beverage.getName(), cloned.getName());
        Assert.assertEquals(Beverage.getPrice(), cloned.getPrice(), 0.001);
        Assert.assertEquals(Beverage.isNonAlcoholic(), cloned.isNonAlcoholic());
    }

    @Test
    public void testEquals() throws Exception {
        Beverage Beverage = productFactory.createBeverage("bev", "Cola", 10, true);
        Beverage equalBeverage = productFactory.createBeverage("bev", "Cola", 10, true);
        Beverage equalBeverageExceptPrice = productFactory.createBeverage("bev", "Cola", 15, true);
        Beverage notEqualBeverage = productFactory.createBeverage("bev2", "Sprite", 10, true);

        assertTrue(Beverage.equals(equalBeverage));
        Assert.assertTrue(equalBeverage.equals(Beverage));
        Assert.assertTrue(Beverage.equals(equalBeverageExceptPrice));
        Assert.assertFalse(Beverage.equals(notEqualBeverage));

    }

}
