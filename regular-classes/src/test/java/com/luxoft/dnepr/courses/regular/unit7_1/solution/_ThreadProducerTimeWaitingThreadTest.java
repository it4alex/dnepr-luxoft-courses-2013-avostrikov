package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

public class _ThreadProducerTimeWaitingThreadTest {

    @Test
    public void testGetTimedWaitingThread() throws Exception {
        Thread thread = ThreadProducer.getTimedWaitingThread();
        Assert.assertEquals(Thread.State.TIMED_WAITING, thread.getState());
    }

}
