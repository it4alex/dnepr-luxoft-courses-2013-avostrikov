package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class _BillTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAppend() throws Exception {
        Bill bill = new Bill();
        //append a piece of bread
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        assertEquals(1, bill.getProducts().size());

        //append the same piece of bread
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        assertEquals(1, bill.getProducts().size());

        //append a beverage
        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        assertEquals(2, bill.getProducts().size());

        //append another beverage
        bill.append(factory.createBeverage("bev2", "Martini", 100, false));
        assertEquals(3, bill.getProducts().size());

        //append the new piece of bread with different price
        bill.append(factory.createBread("br", "Bread", 10, 2.0));
        assertEquals(4, bill.getProducts().size());
    }

    @Test
    public void testSummarize() throws Exception {
        Bill bill = new Bill();
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        bill.append(factory.createBread("br", "Bread", 10, 1.0));

        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        bill.append(factory.createBeverage("bev", "Cola", 50, true));

        bill.append(factory.createBook("book", "Java", 100, new Date()));

        bill.append(factory.createBread("br", "Bread", 15, 2.0));
        assertEquals( (10+10)*0.95 + (10 + 10 + 50) * 0.9 + 100 + 15, bill.summarize(), 0 );
    }

    @Test
    public void testGetProducts() throws Exception {
        Bill bill = new Bill();

        assertEquals(0, bill.getProducts().size());

        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        bill.append(factory.createBread("br", "Bread", 10, 1.0));

        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        bill.append(factory.createBeverage("bev", "Cola", 50, true));

        bill.append(factory.createBook("book", "Java", 100, new Date()));

        List<Product> groupedAndSortedProducts = bill.getProducts();
        assertEquals(3, groupedAndSortedProducts.size());

        //test sorting - by grouped price, descending
        assertTrue(
                groupedAndSortedProducts.get(0).getPrice() >= groupedAndSortedProducts.get(1).getPrice() &&
                        groupedAndSortedProducts.get(1).getPrice() >= groupedAndSortedProducts.get(2).getPrice()
        );

        assertEquals("Java", groupedAndSortedProducts.get(0).getName());
        assertEquals("Cola", groupedAndSortedProducts.get(1).getName());
        assertEquals("Bread", groupedAndSortedProducts.get(2).getName());
    }
}
