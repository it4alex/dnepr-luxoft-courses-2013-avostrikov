package com.luxoft.dnepr.courses.regular.Unit8;

import com.luxoft.dnepr.courses.regular.unit8.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit8.Gender;
import com.luxoft.dnepr.courses.regular.unit8.Person;
import com.luxoft.dnepr.courses.regular.unit8.Serializer;
import junit.framework.Assert;
import org.junit.Test;

import java.io.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/22/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class FamilyTreeTest {
    @Test
    public void FamilyTreeToJsonTest() throws IOException {
        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        FamilyTree familyTree = new FamilyTree(person);

        Serializer.serialize(new File("file.txt"), familyTree);





//        String jsonExpected = "{\"root\":{\"name\":\"Galja\",\"gender\":\"FEMALE\",\"ethnicity\":\"ukrainian\",\"birthDate\":\"May 21, 2013 3:31:33 PM\",\"father\":{\"name\":\"Vasiliy\",\"gender\":\"MALE\",\"ethnicity\":\"ukrainian\",\"birthDate\":\"May 21, 2013 3:31:33 PM\"}}}";
//        Assert.assertTrue(obj.toJson().equals(jsonExpected));
    }
}
