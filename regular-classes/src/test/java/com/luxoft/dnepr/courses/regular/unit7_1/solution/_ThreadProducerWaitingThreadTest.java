package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

public class _ThreadProducerWaitingThreadTest {

    @Test
    public void testGetWaitingThread() throws Exception {
        Assert.assertEquals(Thread.State.WAITING, ThreadProducer.getWaitingThread().getState());
    }

}
