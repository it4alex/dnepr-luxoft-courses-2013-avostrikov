package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;

public class _ThreadProducerTerminatedThreadTest {

    @Test
    public void testGetTerminatedThread() throws Exception {
        Assert.assertEquals(Thread.State.TERMINATED, ThreadProducer.getTerminatedThread().getState());
    }

}
