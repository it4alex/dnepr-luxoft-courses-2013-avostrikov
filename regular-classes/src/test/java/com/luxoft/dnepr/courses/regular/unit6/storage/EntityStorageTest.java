package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.dao.AbstractDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit6.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.model.Redis;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/9/13
 * Time: 7:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorageTest {
    private IDao<Employee> employeeDao;
    private IDao<Redis> redisDao;
    private IDao<Redis> abstractRedisDao;

    @Before
    public void setUp() throws Exception {
        employeeDao = new EmployeeDaoImpl();
        redisDao = new RedisDaoImpl();
        abstractRedisDao = new AbstractDaoImpl<>();


    }

    @After
    public void tearDown() throws Exception {
        //remove all entities from Singleton
        Map<Long, Entity> entities = EntityStorage.getEntities();
        entities.clear();
        employeeDao = null;
        redisDao = null;
        abstractRedisDao = null;

    }

    @Test
    public void testRedisNewId() throws Exception {
        Redis redisWithNullId = new Redis(null, 10);
        Assert.assertTrue(redisDao.save(redisWithNullId).getId().equals(1L));
        redisWithNullId = new Redis(null, 10);
        Assert.assertTrue(redisDao.save(redisWithNullId).getId().equals(2L));
        Redis redisWithId = new Redis(1000L, 1000);
        Assert.assertTrue(redisDao.save(redisWithId).getId().equals(1000L));
        redisWithNullId = new Redis(null, 10);
        Assert.assertTrue(redisDao.save(redisWithNullId).getId().equals(1001L));
    }

    @Test
    public void testConcurrentSave()
    {
        Thread threadWithNullId = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 10; i < 99; i++)
                {
                      redisDao.save(new Redis(EntityStorage.getNextId() , 10));
                }
            }
        });
        threadWithNullId.start();
    }
    @Test
    public void testSave() throws InterruptedException {
        for (int i = 0; i < 5; i++){
            new Thread(){
                public void run(){
                    for (int i = 0; i < 1000; i++) {
                        employeeDao.save(new Employee(EntityStorage.getNextId(), (int)Math.random()*100));
                    }

                }
            }.start();
        }
        Thread.sleep(1000);
//        System.out.println(Collections.max(EntityStorage.getEntities().keySet()));
        Assert.assertEquals(30000, EntityStorage.getEntities().size());

    }

    @Test
    public void testEmployeeNewId() throws Exception {
        Employee employeeWithNullId = new Employee(null, 10);
        Assert.assertTrue(employeeDao.save(employeeWithNullId).getId().equals(1L));
        employeeWithNullId = new Employee(null, 10);
        Assert.assertTrue(employeeDao.save(employeeWithNullId).getId().equals(2L));
        Employee redisWithId = new Employee(1000L, 1000);
        Assert.assertTrue(employeeDao.save(redisWithId).getId().equals(1000L));
        employeeWithNullId = new Employee(null, 10);
        Assert.assertTrue(employeeDao.save(employeeWithNullId).getId().equals(1001L));

    }

    @Test
    public void testEntities() throws Exception {
        Employee employee = new Employee(1L, 500);
        Redis redis = new Redis(2L, 1000);

        redisDao.save(redis);
        employeeDao.save(employee);
        Assert.assertTrue(com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage.getEntities().get(1L) instanceof Employee);
        Assert.assertTrue(com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage.getEntities().get(2L) instanceof Redis);

    }
    @Test
    public void testEntitiesAbstract() throws Exception {
        Redis redis = new Redis(2L, 1000);
        abstractRedisDao.save(redis);
        Assert.assertTrue(EntityStorage.getEntities().get(2L) instanceof Redis);

    }

    @Test(expected = EntityAlreadyExistException.class)
    public void testEmployeeAlreadyExistException() throws Exception {
        Employee employee1 = new Employee(1L, 1000);
        Employee employee2 = new Employee(1L, 1000);
        employeeDao.save(employee1);
        employeeDao.save(employee2);

    }

    @Test(expected = EntityAlreadyExistException.class)
    public void testRedisAlreadyExistException() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        Redis redis2 = new Redis(2L, 2000);
        redisDao.save(redis1);
        redisDao.save(redis2);

    }

    @Test(expected = EntityNotFoundException.class)
    public void testEmployeeUpdateWrongId() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        Employee employee2 = new Employee(1000L, 1000);
        employeeDao.save(employee1);
        employeeDao.update(employee2);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testEmployeeUpdateNullId() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        Employee employee2 = new Employee(null, 1000);
        employeeDao.save(employee1);
        employeeDao.update(employee2);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testEmployeeUpdateNullEmployee() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        employeeDao.save(employee1);
        employeeDao.update(null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRedisUpdateWrongId() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        Redis redis2 = new Redis(2000L, 2000);
        redisDao.save(redis1);
        redisDao.update(redis2);

    }

    @Test(expected = EntityNotFoundException.class)
    public void testRedisUpdateNullId() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        Redis redis2 = new Redis(null, 2000);
        redisDao.save(redis1);
        redisDao.update(redis2);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRedisUpdateNullRedis() throws Exception {
        Redis redis1 = new Redis(2L, 1000);
        redisDao.save(redis1);
        redisDao.update(null);
    }

    @Test
    public void testRedisGet() throws Exception {
        Redis redis1 = new Redis(1L, 1000);
        redisDao.save(redis1);
        Assert.assertTrue(redisDao.get(1000L) == null);
    }

    @Test
    public void testEmployeeGet() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        employeeDao.save(employee1);
        Assert.assertTrue(employeeDao.get(1000L) == null);
    }

    @Test
    public void testRedisDelete() throws Exception {
        Redis redis1 = new Redis(1L, 1000);
        Redis redis2 = new Redis(2L, 1000);
        Redis redis3 = new Redis(3L, 1000);
        Redis redis4 = new Redis(4L, 1000);
        redisDao.save(redis1);
        redisDao.save(redis2);
        redisDao.save(redis3);
        redisDao.save(redis4);


        Assert.assertTrue(redisDao.delete(1L));
        Assert.assertFalse(redisDao.delete(100L));
        Assert.assertFalse(redisDao.delete(100L));
        Assert.assertTrue(redisDao.delete(3L));
        Assert.assertTrue(redisDao.delete(2L));
        Assert.assertTrue(redisDao.delete(4L));
        Assert.assertFalse(redisDao.delete(4L));
    }

    @Test
    public void testEmployeeDelete() throws Exception {
        Employee employee1 = new Employee(1L, 500);
        Employee employee2 = new Employee(2L, 500);
        Employee employee3 = new Employee(3L, 500);
        Employee employee4 = new Employee(4L, 500);
        employeeDao.save(employee1);
        employeeDao.save(employee2);
        employeeDao.save(employee3);
        employeeDao.save(employee4);

        Assert.assertTrue(employeeDao.delete(1L));
        Assert.assertFalse(employeeDao.delete(200L));
        Assert.assertTrue(employeeDao.delete(2L));
        Assert.assertTrue(employeeDao.delete(3L));
        Assert.assertTrue(employeeDao.delete(4L));
        Assert.assertFalse(employeeDao.delete(4L));
    }

    public class RedisDaoRunnable implements Runnable
    {
        private IDao<Redis> redisDao;
        public RedisDaoRunnable(IDao<Redis> dao)
        {
           this.redisDao = dao;
        }

        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p/>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

}
