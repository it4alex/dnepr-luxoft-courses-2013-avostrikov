package com.luxoft.dnepr.courses.regular.unit4;

import junit.framework.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/28/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {
    @Test
    public void testSize() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4"));
        Assert.assertEquals(4, equalSet.size());

        EqualSet<String> equalSetEmpty = new EqualSet<>();
        Assert.assertEquals(0, equalSetEmpty.size());

    }

    @Test
    public void testIsEmpty() throws Exception {
        EqualSet<String> equalSetEmpty = new EqualSet<>();
        Assert.assertTrue(equalSetEmpty.isEmpty());

        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4"));
        Assert.assertFalse(equalSet.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4"));
        Assert.assertTrue(equalSet.contains("3"));
        Assert.assertFalse(equalSet.contains("7"));
        Assert.assertFalse(equalSet.contains(null));

    }

    @Test
    public void testIterator() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4", "5"));
        String[] array = new String[5];
        int i = 0;
        for (String str : equalSet) {
            array[i] = str;
            i++;
        }
        Assert.assertEquals("[1, 2, 3, 4, 5]", Arrays.toString(array));


    }

    @Test
    public void testToArray() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", null, "", "", "", "", "5"));
        Assert.assertEquals("[1, 2, 3, null, , 5]", Arrays.toString(equalSet.toArray()));
    }

    @Test
    public void testCreateWithCollection() {

        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "", "", "", "", "", null, "6"));
        Assert.assertEquals(6, equalSet.size());
    }

    @Test
    public void testAdd() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>();
        equalSet.add("1");
        equalSet.add("1");
        equalSet.add("1");
        equalSet.add("4");
        equalSet.add("5");
        equalSet.add("6");
        equalSet.add("7");
        equalSet.add("8");
        equalSet.add("9");
        equalSet.add(null);
        equalSet.add(null);
        equalSet.add("10");
        equalSet.add("10");
        equalSet.add("11");
        Assert.assertEquals(10, equalSet.size());

    }

    @Test
    public void testRemoveByValue() throws Exception {

        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "3", "4", "5"));
        equalSet.remove("5");
        equalSet.remove("5");
        equalSet.remove("1");
        equalSet.remove("3");
        equalSet.remove("4");
        Assert.assertEquals(1, equalSet.size());
        equalSet.remove("2");
        Assert.assertEquals(0, equalSet.size());
        equalSet.remove("2");
        Assert.assertEquals(0, equalSet.size());
    }

    @Test
    public void testRemoveByIndex() throws Exception {

        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "3", "4", "5"));
        equalSet.remove(4);
        equalSet.remove(3);
        Assert.assertEquals(3, equalSet.size());
        equalSet.remove(2);
        equalSet.remove(1);
        equalSet.remove(0);
        Assert.assertEquals(0, equalSet.size());
    }

    @Test
    public void testContainsAll() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4", "5"));

        Assert.assertTrue(equalSet.containsAll(Arrays.asList("2", "3", "4")));
        Assert.assertTrue(equalSet.containsAll(Arrays.asList("3", "4", "2")));
        Assert.assertFalse(equalSet.containsAll(Arrays.asList("7", "4", "2")));


    }

    @Test
    public void testAddAll() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4", "5"));
        EqualSet<String> equalSet2 = new EqualSet<>(Arrays.asList("1", "2", "3", "6", "7"));
        Assert.assertTrue(equalSet.addAll(equalSet2));
        Assert.assertEquals("[1, 2, 3, 4, 5, 6, 7]", Arrays.deepToString(equalSet.toArray()));
    }

    @Test
    public void testRetainAll() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4", "5", "8", "9", "10"));
        EqualSet<String> equalSet2 = new EqualSet<>(Arrays.asList("1", "2", "3", "6", "7"));
        Assert.assertTrue(equalSet.retainAll(equalSet2));
        Assert.assertEquals("[1, 2, 3]", Arrays.deepToString(equalSet.toArray()));

    }

    @Test
    public void testRemoveAll() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "4", "5"));
        EqualSet<String> equalSet2 = new EqualSet<>(Arrays.asList("1", "2", "3", "6", "7"));
        Assert.assertTrue(equalSet.removeAll(equalSet2));
        Assert.assertEquals("[4, 5]", Arrays.deepToString(equalSet.toArray()));
    }

    @Test
    public void testClear() throws Exception {
        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "3", "4", "5"));
        equalSet.clear();
        Assert.assertEquals(0, equalSet.size());

        equalSet = new EqualSet<>(Arrays.asList("1"));
        equalSet.clear();
        Assert.assertEquals(0, equalSet.size());

        equalSet = new EqualSet<>();
        equalSet.clear();
        Assert.assertEquals(0, equalSet.size());
    }

//    @Test
//    public void toArray() throws Exception {
//        EqualSet<String> equalSet = new EqualSet<>(Arrays.asList("1", "2", "3", "3", "4", "5"));
//        Assert.assertEquals("[1, 2, 3, 4, 5]", Arrays.deepToString(equalSet.toArray()));
//    }

}
