package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/24/13
 * Time: 11:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {
    @Test(expected = WalletIsBlockedException.class)
    public void testCheckWithdrawalBlocked() throws WalletIsBlockedException, LimitExceededException {
        Wallet wallet = new Wallet(1L, new BigDecimal(50), WalletStatus.BLOCKED, new BigDecimal(50));
        wallet.checkTransfer(new BigDecimal(10));
    }

    @Test(expected = InsufficientWalletAmountException.class)
    public void testCheckWithdrawalInsufficientMoney() throws WalletIsBlockedException, LimitExceededException,
            InsufficientWalletAmountException {
        Wallet wallet = new Wallet(1L, new BigDecimal(50), WalletStatus.ACTIVE, new BigDecimal(50));
        wallet.checkWithdrawal(new BigDecimal(100));
    }

    @Test
    public void testAmountToWithdraw() throws WalletIsBlockedException, InsufficientWalletAmountException {
        Wallet wallet = new Wallet(1L, new BigDecimal(100), WalletStatus.ACTIVE, new BigDecimal(200));
        wallet.withdraw(new BigDecimal(40));
        Assert.assertEquals(0, wallet.getAmount().compareTo(new BigDecimal(60)));
    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckTransferBlocked() throws WalletIsBlockedException, LimitExceededException {
        Wallet wallet = new Wallet(1L, new BigDecimal(100), WalletStatus.BLOCKED, new BigDecimal(200));
        wallet.checkTransfer(new BigDecimal(300));
    }

    @Test(expected = LimitExceededException.class)
    public void testCheckTransferLimitExceeded() throws WalletIsBlockedException, LimitExceededException {
        Wallet wallet = new Wallet(1L, new BigDecimal(100), WalletStatus.ACTIVE, new BigDecimal(200));
        wallet.checkTransfer(new BigDecimal(300));
    }

    @Test
    public void testTransfer() throws WalletIsBlockedException, LimitExceededException {
        Wallet wallet = new Wallet(1L, new BigDecimal(100), WalletStatus.ACTIVE, new BigDecimal(200));
        wallet.transfer(new BigDecimal(50));
        Assert.assertEquals(0, wallet.getAmount().compareTo(new BigDecimal(150)));

    }


}
