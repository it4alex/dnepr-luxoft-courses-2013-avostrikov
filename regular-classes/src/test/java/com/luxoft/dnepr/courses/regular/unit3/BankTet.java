package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/24/13
 * Time: 12:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class BankTet {
    private Bank bank;
    private Map<Long, UserInterface> users;

    @Before
    public void setUp() throws Exception {
//        public Wallet(Long id, BigDecimal amount, WalletStatus status,BigDecimal maxAmount)
        Wallet user1Wallet = new Wallet(1L, new BigDecimal(10), WalletStatus.ACTIVE, new BigDecimal(100));
        User user1 = new User(1L, "User1", user1Wallet);

        Wallet user2Wallet = new Wallet(2L, new BigDecimal(20), WalletStatus.ACTIVE, new BigDecimal(25));
        User user2 = new User(2L, "User2", user2Wallet);

        Wallet user3Wallet = new Wallet(3L, new BigDecimal(20), WalletStatus.BLOCKED, new BigDecimal(100));
        User user3 = new User(3L, "User3", user3Wallet);

        users = new HashMap<>();
        users.put(user1.getId(), user1);
        users.put(user2.getId(), user2);
        users.put(user3.getId(), user3);

        bank = new Bank(System.getProperty("java.version"));
        bank.setUsers(users);
    }

    @After
    public void tearDown() throws Exception {
        users.clear();
        bank = null;

    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionLimitExceeded() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(1L, 2L, new BigDecimal(8));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionInsufficientAmount() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(1L, 2L, new BigDecimal(15));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionBlockedWallet() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(1L, 3L, new BigDecimal(1));
    }

    @Test(expected = NoUserFoundException.class)
    public void testMakeMoneyTransactionWrongUser() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(7L, 31L, new BigDecimal(500));
    }

    @Test
    public void testBankVersionJavaOK() {
        System.out.println(System.getProperty("java.version"));
        Bank testBank = new Bank(System.getProperty("java.version"));
    }

    @Test(expected = IllegalJavaVersionError.class)
    public void testBankVersionJavaWrong() {
        Bank testBank;
        testBank = new Bank(null);
    }
}
