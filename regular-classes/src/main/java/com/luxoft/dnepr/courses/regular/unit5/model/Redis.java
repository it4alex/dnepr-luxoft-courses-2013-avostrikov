package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class Redis extends Entity {
    private int weight;

    public Redis() {
    }

    public Redis(Long id) {
        super(id);
    }

    public Redis(Long id, int weight) {
        super(id);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Redis redis = (Redis) o;

        return Double.compare(redis.weight, weight) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Double.valueOf(weight).hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Redis{" + super.toString() +
                " weight=" + weight +
                '}';
    }
}