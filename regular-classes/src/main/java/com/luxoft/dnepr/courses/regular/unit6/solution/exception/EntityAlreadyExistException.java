package com.luxoft.dnepr.courses.regular.unit6.exception;

public class EntityAlreadyExistException extends RuntimeException {

    public EntityAlreadyExistException() { }

    public EntityAlreadyExistException(String message) {
        super(message);
    }
}
