package com.luxoft.dnepr.courses.regular.unit2.solution;

import java.util.Date;
import java.util.Objects;

public class Book extends AbstractProduct {
    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;
        return Objects.equals(publicationDate, book.publicationDate);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Objects.hashCode(publicationDate);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book clonedBook = (Book)super.clone();
        if (publicationDate != null) {
            clonedBook.publicationDate = (Date)publicationDate.clone();
        }
        return clonedBook;
    }
}
