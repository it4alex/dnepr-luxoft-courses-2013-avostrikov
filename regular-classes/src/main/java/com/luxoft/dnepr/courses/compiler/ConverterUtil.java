package com.luxoft.dnepr.courses.compiler;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/18/13
 * Time: 1:11 AM
 * To change this template use File | Settings | File Templates.
 */

class ConverterUtil {
    private static final LinkedList<String> operators =
            new LinkedList<String>(Arrays.asList("+","-","*","/"));

    private ConverterUtil() {
    }

    public static boolean isOperand(String s) {
        return s.matches("([\\d]+[.]?+([\\d]+)?)");
    }

    public static boolean isOperator(String s) {
        return s.matches("[\\+\\-\\*\\/]");
    }

    private static int precedence(String operator) {
        return operators.indexOf(operator);
    }

    public static String infixToPostfix(String input) {
        LinkedList<String> list = new LinkedList<String>();
        StringBuilder result = new StringBuilder();
        String space = " ";
        String[] elements = input.split("\\s");

        for (String element : elements) {

            if (isOperand(element)) {
                result.append(element).append(space);
            } else if (element.equals("(")) {
                list.push(element);
            } else if (isOperator(element)) {
                while (!list.isEmpty() && precedence(list.peek()) >= precedence(element)) {
                    result.append(list.pop()).append(space);
                }
                list.push(element);
            } else if (element.equals(")")) {
                while (!list.peek().equals("(")) {
                    result.append(list.pop()).append(space);
                }
                list.pop();
            }
        }
        while (!list.isEmpty()) {
            result.append(list.pop()).append(space);
        }
        return result.toString();
    }
}

