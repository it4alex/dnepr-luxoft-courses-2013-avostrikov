package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class RedisDaoImpl implements IDao<Redis> {
    private Map<Long, Entity> entities = EntityStorage.getEntities();

    @Override
    public Redis save(Redis redis) throws UserAlreadyExist {
        if (redis.getId() == null) {
            redis.setId(EntityStorage.getNextId());
        }
        if (entities.containsKey(redis.getId()))
            throw new UserAlreadyExist(redis.toString() + " already in storage");
        entities.put(redis.getId(), redis);
        return redis;
    }

    @Override
    public Redis update(Redis redis) throws UserNotFound {
        if (redis == null || redis.getId() == null || !entities.containsKey(redis.getId()))
            throw new UserNotFound();
        entities.put(redis.getId(), redis);
        return (Redis) entities.get(redis.getId());
    }

    @Override
    public Redis get(long id) {
        if (!entities.containsKey(id)) return null;
        return (Redis) entities.get(id);
    }

    @Override
    public boolean delete(long id) {
        Entity entity = entities.remove(id);
        return (entity != null);
    }
}
