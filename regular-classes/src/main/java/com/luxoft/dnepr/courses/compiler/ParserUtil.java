package com.luxoft.dnepr.courses.compiler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/17/13
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParserUtil {
    private ParserUtil() {
    }

    public static String replaceAllByPattern(String source, String pattern, String replacer) {

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(source);
        if (m.find())
            return m.replaceAll(replacer);
        return source;
    }

    public static String removeSpaces(String source) {
        return replaceAllByPattern(source, "\\s", "");
    }

    public static String addSpaces(String source) {
        StringBuilder result = new StringBuilder();
        Pattern p = Pattern.compile("([\\d]+[.]?+([\\d]+)?)|[\\(\\)\\+\\-\\*\\/]");
        Matcher m = p.matcher(source);
        while (m.find()) {
            result.append(source.substring(m.start(), m.end()));
            if (m.end() < source.length()) result.append(" ");
        }
        return result.toString();

    }

}
