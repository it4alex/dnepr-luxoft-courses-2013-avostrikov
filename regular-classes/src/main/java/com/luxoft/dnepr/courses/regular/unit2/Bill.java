package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private ArrayList<CompositeProduct> compositeProducts = new ArrayList<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        boolean productInList = false;

        if (!compositeProducts.isEmpty()) {
            for (CompositeProduct compositeProduct : compositeProducts) {
                if (compositeProduct.getChildProducts().isEmpty()) {
                    compositeProduct.add(product);
                    productInList = true;
                } else if (compositeProduct.getProduct() != null) {
                    if (compositeProduct.getProduct().equals(product)) {
                        compositeProduct.add(product);
                        productInList = true;
                    }
                }
            }
        }

        if (compositeProducts.isEmpty() || !productInList) {
            CompositeProduct cp = new CompositeProduct();
            cp.add(product);
            compositeProducts.add(cp);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return total
     */
    public double summarize() {
        double totalBill = 0;

        for (CompositeProduct compositeProduct : compositeProducts) {
            totalBill += compositeProduct.getPrice();
        }

        return totalBill;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return ordered list
     */
    public List<Product> getProducts() {
        ArrayList<Product> productsAsBill = new ArrayList<>();

        CompareProduct compareProduct = new CompareProduct();
        Collections.sort(compositeProducts, compareProduct);

        for (CompositeProduct compositeProduct : compositeProducts) {
            AbstractProduct product = (AbstractProduct) compositeProduct.getProduct();
            product.setPrice(compositeProduct.getPrice());
            productsAsBill.add(product);
        }

        return productsAsBill;

    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<>();
        for (CompositeProduct compositeProduct : compositeProducts) {
            productInfos.add(compositeProduct.toString() + "\n");

        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    private class CompareProduct implements Comparator<CompositeProduct> {

        @Override
        public int compare(CompositeProduct o1, CompositeProduct o2) {
            if (o1.getPrice() == o2.getPrice()) return 0;
            if (o1.getPrice() > o2.getPrice()) return -1;
            else return 1;
        }
    }

}
