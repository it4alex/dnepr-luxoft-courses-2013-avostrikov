package com.luxoft.dnepr.courses.regular.unit5.solution.storage;



import com.luxoft.dnepr.courses.regular.unit5.solution.model.Entity;

import java.util.HashMap;
import java.util.Map;

public class EntityStorage {

    private final static Map<Long, Entity> entities = new HashMap();

    private EntityStorage() {}

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

}
