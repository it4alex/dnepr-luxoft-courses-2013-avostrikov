package com.luxoft.dnepr.courses.regular.unit2.solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     * @return product code
     */
    @Override
    public String getCode() {
        if (getAmount() == 0) {
            return null;
        }
        return childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     * @return product name
     */
    @Override
    public String getName() {
        if (getAmount() == 0) {
            return null;
        }
        return childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        if (getAmount() == 0) {
            return 0;
        }
        double totalPrice = 0;
        for (Product childProduct : childProducts) {
            totalPrice += childProduct.getPrice();
        }
        return totalPrice * getDiscountRatio(getAmount());
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    private double getDiscountRatio(int amount) {
        switch(amount) {
            case 1: return 1;
            case 2: return 0.95;
            default: return 0.9;
        }
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
