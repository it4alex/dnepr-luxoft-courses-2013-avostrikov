package com.luxoft.dnepr.courses.regular.unit7_1;

public final class ThreadProducer {

    private static final long DELAY_IN_MS = 1000;

    private ThreadProducer() {
    }

    public static Thread getNewThread() {
        return new Thread();
    }

    public static Thread getRunnableThread() {
        Thread t = new RunnableThread();
        t.start();
        return t;
    }

    public static Thread getBlockedThread() {
        Thread t1 = new BlockingThread();
        t1.start();
        try {
            Thread.sleep(DELAY_IN_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1 = new BlockingThread();
        t1.start();
        try {
            Thread.sleep(DELAY_IN_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /*t1 = new BlockingThread();
        t1.start();
        try {
            Thread.sleep(DELAY_IN_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return t1;
    }

    public static Thread getWaitingThread() {
        Thread t1 = new WaitingThread();
        t1.start();
        try {
            Thread.sleep(DELAY_IN_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return t1;
    }

    public static Thread getTimedWaitingThread() {
       Thread t = new TimedWaitingThread();
        t.start();
        try {
            Thread.sleep(DELAY_IN_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return t;
    }

    public static Thread getTerminatedThread() {
        Thread t = null;
        try {
            t = new Thread();
            t.start();
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return t;
    }

    public static synchronized void waitingFunction() throws InterruptedException {
        ThreadProducer.class.wait();
    }

    public static synchronized void blockingFunction() {
        while (true) {

        }
    }


    private static class RunnableThread extends Thread {
        public void run() {
            while (true) {
            }
        }
    }

    private static class WaitingThread extends Thread {
        public void run() {
            try {
                ThreadProducer.waitingFunction();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private static class BlockingThread extends Thread {
        public void run() {
            ThreadProducer.blockingFunction();
        }
    }

    private static class TimedWaitingThread extends Thread {
        public void run() {
            while (true) {
                try {
                    Thread.sleep(1000000);
                } catch (InterruptedException e) {
                    System.out.println(String.format("Thread '%s' has been interrupted", this.getName()));
                    break;
                }
            }
        }
    }

}