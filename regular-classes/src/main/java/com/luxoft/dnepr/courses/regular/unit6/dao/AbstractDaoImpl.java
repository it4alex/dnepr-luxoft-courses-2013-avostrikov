package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractDaoImpl<T extends Entity> implements IDao<T> {
    private Map<Long, Entity> entities = EntityStorage.getEntities();
    private Lock lock = new ReentrantLock();

    @Override
    public T save(T t) throws EntityAlreadyExistException {
        synchronized (entities) {
            if (t.getId() == null) {
                t.setId(EntityStorage.getNextId());
            }

            if (entities.containsKey(t.getId()))
                throw new EntityAlreadyExistException(t.toString() + " already in storage");
            else
                entities.put(t.getId(), t);

            return (T) entities.get(t.getId());
        }

    }

    @Override
    public T update(T t) throws EntityNotFoundException {
        if (t == null || t.getId() == null || !entities.containsKey(t.getId()))
            throw new EntityNotFoundException();
        entities.put(t.getId(), t);
        return (T) entities.get(t.getId());

    }

    @Override
    public synchronized T get(long id) {
        if (!entities.containsKey(id)) return null;
        T entity = (T) entities.get(id);
        return entity;
    }

    @Override
    public boolean delete(long id) {
        Entity entity = entities.remove(id);
        return (entity != null);
    }
}
