package com.luxoft.dnepr.courses.regular.unit2.solution;

import java.util.*;

/**
 * Represents a bill.
 * Combines {@link Product}s and provides information about total price.
 */
public class Bill {
    private Map<Product, CompositeProduct> product2CompositeMap = new HashMap<Product, CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     * @param product new product
     */
    public void append(Product product) {
        CompositeProduct compositeProduct = product2CompositeMap.get(product);
        if (compositeProduct == null) {
            compositeProduct = new CompositeProduct();
            product2CompositeMap.put(product, compositeProduct);
        }
        compositeProduct.add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * @return
     */
    public double summarize() {
        double totalPrice = 0;
        for (CompositeProduct compositeProduct : product2CompositeMap.values()) {
            totalPrice += compositeProduct.getPrice();
        }
        return totalPrice;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * @return
     */
    public List<Product> getProducts() {
        List<Product> products = new ArrayList<Product>(product2CompositeMap.values());
        Collections.sort(products, new ProductComparator());
        return products;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize() ;
    }

    /**
     * Custom comparator for {@link Product}.
     * Compares products by their prices.
     */
    protected static class ProductComparator implements Comparator<Product> {

        @Override
        public int compare(Product p1, Product p2) {
            return Double.compare(p2.getPrice(), p1.getPrice());
        }
    }
}
