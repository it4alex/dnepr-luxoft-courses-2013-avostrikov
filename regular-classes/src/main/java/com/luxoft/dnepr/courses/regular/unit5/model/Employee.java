package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class Employee extends Entity {
    private int salary;

    public Employee() {
    }

    public Employee(Long id) {
        super(id);
    }

    public Employee(Long id, int salary) {
        super(id);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Employee employee = (Employee) o;

        return salary == employee.salary;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (salary);
        return result;
    }

    @Override
    public String toString() {
        return "Employee{" + super.toString() +
                " salary=" + salary +
                '}';
    }
}