package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractDaoImpl<T extends Entity> implements IDao<T> {
    private Map<Long, Entity> entities = EntityStorage.getEntities();

    @Override
    public T save(T t) throws UserAlreadyExist {
        if (t.getId() == null) {
            t.setId(EntityStorage.getNextId());
        }
        if (entities.containsKey(t.getId()))
            throw new UserAlreadyExist(t.toString() + " already in storage");
        entities.put(t.getId(), t);
        return (T) entities.get(t.getId());
    }

    @Override
    public T update(T t) throws UserNotFound {
        if (t == null || t.getId() == null || !entities.containsKey(t.getId()))
            throw new UserNotFound();
        entities.put(t.getId(), t);
        return (T)entities.get(t.getId());

    }

    @Override
    public T get(long id) {
        if (!entities.containsKey(id)) return null;
        T entity = (T) entities.get(id);
        return entity;
    }

    @Override
    public boolean delete(long id) {
        Entity entity = entities.remove(id);
        return (entity != null);
    }
}
