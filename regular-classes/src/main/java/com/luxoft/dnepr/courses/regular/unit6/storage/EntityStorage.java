package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new ConcurrentHashMap<>();
//    private final static Map<Long, Entity> entities = Collections.synchronizedMap(new HashMap<Long, Entity>());
    private static Lock lock = new ReentrantLock();
    private static AtomicLong generator = new AtomicLong();
//    private static Lock generatorLock = ReentrantLock();

    private EntityStorage() {
//        Collections.synchronizedMap(entities);
    }

    public static  Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long getNextId() {

        synchronized (entities){

//        if (entities.size() == 0) generator.set(1L);
//        else
//
//            generator.set((Collections.max(entities.keySet()) >= generator.get()) ?
//                    Collections.max(entities.keySet()) + 1 : generator.incrementAndGet());
//        return generator.get();
//        return generator.incrementAndGet();
        return (entities.size() == 0)?1L:Collections.max(entities.keySet()) + 1;

        }

    }
}
