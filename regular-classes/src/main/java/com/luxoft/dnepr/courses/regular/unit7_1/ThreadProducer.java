package com.luxoft.dnepr.courses.regular.unit7_1;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/16/13
 * Time: 6:41 AM
 * To change this template use File | Settings | File Templates.
 */
public final class ThreadProducer {
    private ThreadProducer() {
    }

    public static Thread getNewThread() {
        Thread thread = new Thread(new MyRannable());
        return thread;
    }

    public static Thread getRunnableThread() {
        Thread thread = new Thread(new MyRannable());
        thread.start();
        return thread;
    }

    public static Thread getBlockedThread() {
        Thread thread1 = new Thread(new MyRannable() {
            @Override
            public void run() {
                MyRannable.syncCycleMethod();
            }
        });
        Thread thread2 = new Thread(new MyRannable() {
            @Override
            public void run() {
                MyRannable.syncCycleMethod();
            }
        });

        thread1.start();
        thread2.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        return (thread1.getState() == Thread.State.BLOCKED) ? thread1 : thread2;
    }

    public static Thread getWaitingThread() {
        Thread thread1 = new Thread(new MyRannable() {
            @Override
            public void run() {
                MyRannable.lockingWithObjectForWaiting(null);
            }
        });

        thread1.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        return thread1;
    }

    public static Thread getTimedWaitingThread() {
        Thread thread1 = new Thread(new MyRannable() {
            @Override
            public void run() {
                MyRannable.lockingWithObjectForWaiting(5000L);
            }
        });

        thread1.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        return thread1;
    }

    public static Thread getTerminatedThread() {
        Thread thread = new Thread(new MyRannable());
        thread.start();
        thread.interrupt();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        return thread;
    }

    public static void printState(Thread thread) {
        switch (thread.getState()) {
            case NEW:
                System.out.println("Thread status is New");
                break;
            case RUNNABLE:
                System.out.println("Thread status is Runnable");
                break;
            case BLOCKED:
                System.out.println("Thread status is Blocked");
                break;
            case WAITING:
                System.out.println("Thread status is Waiting");
                break;
            case TIMED_WAITING:
                System.out.println("Thread status is Timed Waiting");
                break;
            case TERMINATED:
                System.out.println("Thread status is Terminated");
                break;
            default:
                System.out.println("Wrong status!");
        }
    }

    public static class MyRannable implements Runnable {
        private static String holder = new String("Simple String locking object");

        public static void lockingWithObjectForWaiting(Long time) {
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (holder) {
                    try {
                        if (time == null || time == 0)
                            holder.wait();
                        else holder.wait(time);

                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public static synchronized void syncCycleMethod() {
            while (!Thread.currentThread().isInterrupted()) {
            }
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
            }
        }


    }
}
