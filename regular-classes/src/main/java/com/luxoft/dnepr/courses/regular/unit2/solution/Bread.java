package com.luxoft.dnepr.courses.regular.unit2.solution;

public class Bread extends AbstractProduct {
    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Bread bread = (Bread) o;

        return Double.compare(bread.weight, weight) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Double.valueOf(weight).hashCode();
        return result;
    }
}
