package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person implements Externalizable {
    private static final long serialVersionUID = 1L;
    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;

    public Person() {
    }

    public Person(String name, Gender gender, String ethnicity, Date birthDate, Person father, Person mother) {
        this.name = name;
        this.gender = gender;
        this.ethnicity = ethnicity;
        this.birthDate = birthDate;
        this.father = father;
        this.mother = mother;
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        String tmp;
        in.readUTF();
        name = in.readUTF();
        in.readUTF();
        in.readUTF();

        gender = Gender.valueOf(in.readUTF());
        in.readUTF();
        in.readUTF();
        ethnicity = new String(in.readUTF());
        in.readUTF();
        in.readUTF();

        String stringDate = in.readUTF();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            birthDate = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        in.readUTF();
        tmp = (String) in.readUTF();
        if (tmp.equals("\"father\":\"")) {
            father = new Person();
            father.readExternal(in);
            in.readUTF();
        }
        in.readUTF();
        if (tmp.equals("\"mother\":\"")) {
            mother = new Person();
            mother.readExternal(in);
        }
        in.readUTF();

    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

        out.writeUTF("{\"name\":\"");
        out.writeUTF(this.name);
        out.writeUTF("\",");
        out.writeUTF("\"gender\":\"");
        out.writeUTF(this.gender.toString());
        out.writeUTF("\",");
        out.writeUTF("\"ethnicity\":\"");
        out.writeUTF(this.ethnicity);
        out.writeUTF("\",");
        out.writeUTF("\"birthDate\":\"");
        out.writeUTF(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.birthDate));
        out.writeUTF("\",");
        if (this.father == null) {
            out.writeUTF("\"father\":\"\",");
        } else {
            out.writeUTF("\"father\":\"");
            father.writeExternal(out);
            out.writeUTF("\",");
        }
        if (this.mother == null) {
            out.writeUTF("\"mother\":\"\"");
        } else {
            out.writeUTF("\"mother\":\"");

            mother.writeExternal(out);
            out.writeUTF("\"");
        }
        out.writeUTF("}");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public String toJson() {
        return null;
    }
}
