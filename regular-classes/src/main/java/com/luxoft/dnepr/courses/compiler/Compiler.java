package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));

        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    private static void addOperator(ByteArrayOutputStream result, String operator) {
        if (operator.equals("+")) {
            addCommand(result, VirtualMachine.ADD);

        } else if (operator.equals("-")) {
            addCommand(result, VirtualMachine.SWAP);
            addCommand(result, VirtualMachine.SUB);

        } else if (operator.equals("*")) {
            addCommand(result, VirtualMachine.MUL);

        } else if (operator.equals("/")) {
            addCommand(result, VirtualMachine.SWAP);
            addCommand(result, VirtualMachine.DIV);


        } else {
            throw new IllegalArgumentException("Compiler.addOperator can't find operator:" + operator);
        }
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream stack = new ByteArrayOutputStream();

        if (input == null || input.isEmpty())
            throw new IllegalArgumentException("Compiler.compile(String): String parameter is empty or null");
        input = ParserUtil.removeSpaces(input);
            input = ParserUtil.addSpaces(input);

            String postfix = ConverterUtil.infixToPostfix(input);
            String[] elements = postfix.split(" ");
            System.out.println(Arrays.toString(elements));
            for (String element : elements) {
                if (ConverterUtil.isOperand(element))
                    addCommand(stack, VirtualMachine.PUSH, Double.parseDouble(element));
                else addOperator(stack, element);

            }
            addCommand(stack, VirtualMachine.PRINT);

        return stack.toByteArray();

    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
