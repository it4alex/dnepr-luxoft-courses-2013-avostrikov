package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


public abstract class AbstractDaoImpl<E extends Entity> implements IDao<E> {

    public static final AtomicLong SAVE_HITS = new AtomicLong();

    @Override
    public synchronized E save(E e) throws EntityAlreadyExistException {
        if (e.getId() != null) {
            Entity entity = EntityStorage.getEntities().get(e.getId());
            if (entity != null && entity!= EntityStorage.NULL_VALUE) {
                throw new EntityAlreadyExistException();
            }
        }
        long id = EntityStorage.getNextId();
        e.setId(id);
        EntityStorage.getEntities().put(id, e);
        System.out.println(String.format("SAVE_HITS = %s, id = %s", SAVE_HITS.addAndGet(1), id));
        return e;
    }

    @Override
    public synchronized E update(E e) throws EntityNotFoundException {
        if (e.getId() == null) {
            throw new EntityNotFoundException();
        }
        E toUpdate = (E) EntityStorage.getEntities().get(e.getId());
        if (toUpdate == null || toUpdate == EntityStorage.NULL_VALUE) {
            throw new EntityNotFoundException();
        }
        EntityStorage.getEntities().put(e.getId(), e);
        return e;
    }

    @Override
    public synchronized E get(long id) {
        return (E) EntityStorage.getEntities().get(id);
    }

    @Override
    public synchronized boolean delete(long id) {
        return EntityStorage.getEntities().remove(id) != null;
    }

}
