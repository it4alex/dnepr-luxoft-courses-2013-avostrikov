package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmployeeDaoImpl implements IDao<Employee> {
    private Map<Long, Entity> entities = EntityStorage.getEntities();

    @Override
    public Employee save(Employee employee) throws UserAlreadyExist {
        if (employee.getId() == null) {
            employee.setId(EntityStorage.getNextId());
        }
        if (entities.containsKey(employee.getId()))
            throw new UserAlreadyExist(employee.toString() + " already in storage");
        entities.put(employee.getId(), employee);
        return (Employee) entities.get(employee.getId());
    }

    @Override
    public Employee update(Employee employee) throws UserNotFound {
        if (employee == null || employee.getId() == null || !entities.containsKey(employee.getId()))
            throw new UserNotFound();
        entities.put(employee.getId(), employee);
        return (Employee) entities.get(employee.getId());

    }

    @Override
    public Employee get(long id) {
        if (!entities.containsKey(id)) return null;
        Employee employee = (Employee) entities.get(id);
        return employee;
    }

    @Override
    public boolean delete(long id) {
        Entity entity = entities.remove(id);
        return (entity != null);
    }
}
