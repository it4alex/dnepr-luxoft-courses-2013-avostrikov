package com.luxoft.dnepr.courses.regular.unit4.solution;

import java.util.*;

public class EqualSet<E> implements Set<E> {

    private List<E> set = new ArrayList();

    public EqualSet() { }

    public EqualSet(Collection<? extends E> collection) {
        this.addAll(collection);
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return set.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return set.iterator();
    }

    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return set.toArray(a);
    }

    @Override
    public boolean add(E e) {
        boolean mustBeAdded = true;
        for (E item : set) {
            if (e == null) {
                if (e == item) {
                    mustBeAdded = false;
                }
            } else {
                if (e.equals(item)) {
                    mustBeAdded = false;
                }
            }
        }
        if (mustBeAdded) {
            set.add(e);
        }
        return mustBeAdded;
    }

    @Override
    public boolean remove(Object o) {
        return set.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return set.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        //COPY-PASTE from AbstractCollection
        boolean modified = false;
        for (E e : c) {
            if (add(e)) {
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return set.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return set.removeAll(c);
    }

    @Override
    public void clear() {
        set.clear();
    }


}
