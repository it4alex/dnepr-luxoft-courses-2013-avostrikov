package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EntityStorage {

    private final static Map<Long, Entity> entities = new ConcurrentHashMap();
    public final static Entity NULL_VALUE = new Entity();

    private EntityStorage() {}

    public synchronized static Map<Long, Entity> getEntities() {
        return entities;
    }

    public synchronized static Long getNextId() {
        long id = 0;
        for (Long key : entities.keySet()) {
            if (key > id) {
                id = key;
            }
        }
        entities.put(id + 1, NULL_VALUE);
        return id + 1;
    }

}
