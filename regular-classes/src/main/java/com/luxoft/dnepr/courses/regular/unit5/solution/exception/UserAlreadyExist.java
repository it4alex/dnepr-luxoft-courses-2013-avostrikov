package com.luxoft.dnepr.courses.regular.unit5.solution.exception;

public class UserAlreadyExist extends Exception {

    public UserAlreadyExist() { }

    public UserAlreadyExist(String message) {
        super(message);
    }
}
