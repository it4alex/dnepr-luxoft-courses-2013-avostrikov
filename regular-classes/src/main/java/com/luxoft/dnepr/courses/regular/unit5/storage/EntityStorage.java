package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();

    private EntityStorage() {
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long getNextId() {
        return (entities.size() == 0)?1L:Collections.max(entities.keySet()) + 1;
    }
}
