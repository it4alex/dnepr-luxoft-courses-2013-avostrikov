package com.luxoft.dnepr.courses.regular.unit5.solution.dao;



import com.luxoft.dnepr.courses.regular.unit5.solution.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.solution.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.solution.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.solution.storage.EntityStorage;

import java.util.Map;


public abstract class AbstractDaoImpl<E extends Entity> implements IDao<E> {

    @Override
    public E save(E e) throws UserAlreadyExist {
        if (e.getId() != null && EntityStorage.getEntities().get(e.getId()) != null) {
            throw new UserAlreadyExist();
        }
        long id = getNextId(EntityStorage.getEntities());
        e.setId(id);
        EntityStorage.getEntities().put(id, e);
        return e;
    }

    @Override
    public E update(E e) throws UserNotFound {
        E toUpdate = (E) EntityStorage.getEntities().get(e.getId());
        if (toUpdate == null) {
            throw new UserNotFound();
        }
        EntityStorage.getEntities().put(e.getId(), e);
        return e;
    }

    @Override
    public E get(long id) {
        return (E) EntityStorage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        return EntityStorage.getEntities().remove(id) != null;
    }

    private static long getNextId(Map<Long, ?> storage) {
        long id = 0;
        for (Long key : storage.keySet()) {
            if (key > id) {
                id = key;
            }
        }
        return id + 1;
    }

}
