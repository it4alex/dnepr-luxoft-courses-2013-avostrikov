package com.luxoft.dnepr.courses.regular.unit5.solution.dao;


import com.luxoft.dnepr.courses.regular.unit5.solution.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.solution.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.solution.model.Entity;

public interface IDao<E extends Entity> {

    E save (E e) throws UserAlreadyExist;
    E update (E e) throws UserNotFound;
    E get (long id);
    boolean delete (long id);

}
