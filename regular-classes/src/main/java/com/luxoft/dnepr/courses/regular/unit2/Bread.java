package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {
    private double Weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        Weight = weight;
    }

    public double getWeight() {
        return Weight;
    }

    public void setWeight(double weight) {
        Weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bread)) return false;
        if (!super.equals(o)) return false;

        Bread bread = (Bread) o;

        return Double.compare(bread.Weight, Weight) == 0;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(Weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return super.getCode() + " " + super.getName() + " " + Weight + " g." +
                " =  $ " + super.getPrice() + "\n";


    }
}
