package com.luxoft.dnepr.courses.regular.unit2.solution;

import java.util.Objects;

/**
 * Represents abstract product.
 * Encapsulates common data and adds general implementation of the {@link Product} and common methods.
 */
public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;

    public AbstractProduct() {
    }

    protected AbstractProduct(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractProduct)) return false;

        AbstractProduct that = (AbstractProduct) o;

        return Objects.equals(code, that.code) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
