package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct {
    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        return !(publicationDate != null ? !publicationDate.equals(book.publicationDate) : book.publicationDate != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

            Book cloned = (Book) super.clone();
            if (this.publicationDate != null)
                cloned.setPublicationDate(new Date(this.getPublicationDate().getTime()));
            return cloned;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(super.getCode() + " " + super.getName() + " ");
        result.append(publicationDate != null ? publicationDate.toString() : "");
        result.append(" =  $ " + super.getPrice() + "\n");

        return result.toString();
    }
}
