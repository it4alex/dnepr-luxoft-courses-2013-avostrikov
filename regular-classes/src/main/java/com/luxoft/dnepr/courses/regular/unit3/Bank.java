package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/24/13
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) {
        String actualJavaVersion = System.getProperty("java.version");
        if (expectedJavaVersion == null) expectedJavaVersion = "";

        if (!actualJavaVersion.equals(expectedJavaVersion))
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Wrong JRE version!");
    }

    public Bank() {
    }

    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    private UserInterface getUser(Long userId) throws NoUserFoundException {
        if (users != null && !users.isEmpty())
            if (!users.containsKey(userId)) throw new NoUserFoundException(userId, "User id=" + userId + " not found");
        return users.get(userId);
    }

    /**
     * Method transfer money from one user account to another
     *
     * @param fromUserId account
     * @param toUserId   account
     * @param amount     money
     * @throws NoUserFoundException
     * @throws TransactionException
     */
    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
        try {
            getUser(fromUserId).getWallet().withdraw(amount);

        } catch (WalletIsBlockedException ex) {
            throw new TransactionException("User '" + getUser(fromUserId).getName() + "' wallet is blocked");
        } catch (InsufficientWalletAmountException ex) {
            throw new TransactionException("User '" + getUser(fromUserId).getName() + "' has insufficient " +
                    "funds (" + getUser(fromUserId).getWallet().getAmount() + " < "
                    + amount + ")");
        }

        try {
            getUser(toUserId).getWallet().transfer(amount);

        } catch (WalletIsBlockedException ex) {
            throw new TransactionException("User '" + getUser(toUserId).getName() + "' wallet is blocked");
        } catch (LimitExceededException ex) {
            throw new TransactionException("User '" + getUser(toUserId).getName() + "' wallet limit " +
                    "exceeded (" + getUser(toUserId).getWallet().getAmount() +
                    " + " + amount + " > " + getUser(toUserId).getWallet().getMaxAmount() + ")");
        }
    }
}
