package com.luxoft.dnepr.courses.regular.unit6.model;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/8/13
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class Entity {

    private Long id;

    public Entity() {
    }

    public Entity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;

        Entity that = (Entity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "id=" + id;
    }
}
