package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 5/22/13
 * Time: 9:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class Serializer {
    public static void serialize(File file, FamilyTree entity) {

        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            entity.writeExternal(oos);

        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
        } catch (IOException ex) {
            System.out.println("Input-output exception.");
        }


    }

    public static FamilyTree deserialize(File file) {
        FamilyTree familyTree = new FamilyTree();

        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis);) {

            familyTree = (FamilyTree) ois.readObject();

        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        } catch (IOException e) {
            System.err.println("Input-output exception.");
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found exception.");
        }

        return familyTree;
    }
}

