package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (childProducts.isEmpty()) return null;
        return childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (childProducts.isEmpty()) return null;
        return childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double totalPrice = 0;
        for (Product product : childProducts) {
            totalPrice += product.getPrice();

        }
        if (getAmount() == 0) return 0;
        if (getAmount() == 1) return totalPrice;
        if (getAmount() == 2) return totalPrice * 0.95;
        else
            return totalPrice * 0.9;

    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    public List<Product> getChildProducts() {
        return childProducts;
    }

    public Product getProduct() {
        if (childProducts.isEmpty()) return null;
        else return childProducts.get(0);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
