package com.luxoft.dnepr.courses.regular.unit2.solution;

public class Beverage extends AbstractProduct {
    private boolean nonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        return nonAlcoholic == beverage.nonAlcoholic;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }
}
