package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/21/13
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;

    protected AbstractProduct() {
    }

    protected AbstractProduct(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if ((o == null) || (o.getClass() != this.getClass())) return false;
        AbstractProduct abstractProduct = (AbstractProduct) o;

//        if (code != abstractProduct.code || code == null || !code.equals(abstractProduct.code)) return false;
//        return !(name != abstractProduct.name || name == null || !name.equals(abstractProduct.name));

        return Objects.equals(name, abstractProduct.name) && Objects.equals(code, abstractProduct.code);
    }

    @Override
    public int hashCode() {
//        int hash = 7;
//        long temp;
//        hash = 31 * hash + (null == name ? 0 : name.hashCode());
//        hash = 31 * hash + (null == code ? 0 : code.hashCode());
////--        temp = Double.doubleToLongBits(price);
////--        hash = 31 * hash + (int) (temp ^ (temp >>> 32));
//        return hash;

        return Objects.hash(code, name);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

//        AbstractProduct cloned = (AbstractProduct) super.clone();
//        cloned.setPrice(this.price);
//        if (this.code != null) cloned.setCode(this.code);
//        if (this.name != null) cloned.setName(this.name);
//        return cloned;
        return super.clone();

    }

    @Override
    public String toString() {
        return code + " " + " " + name + " " + price;

    }
}
