package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: Vostrykov Olexiy
 * Date: 4/24/13
 * Time: 6:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet() {
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        super();
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
        this.maxAmount = maxAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
        checkInconsistency();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
        checkInconsistency();
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
        checkInconsistency();
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException,
            InsufficientWalletAmountException {
        if (this.status == WalletStatus.BLOCKED)
            throw new WalletIsBlockedException(this.id, "This wallet is blocked for withdrawal");
        if (amountToWithdraw.compareTo(this.amount) > 0)
            throw new InsufficientWalletAmountException(this.id, amountToWithdraw, amount,
                    "Not enough money in the wallet to withdrawal");
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        checkWithdrawal(amountToWithdraw);
        amount = amount.subtract(amountToWithdraw);

    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (this.status == WalletStatus.BLOCKED)
            throw new WalletIsBlockedException(this.id, "This wallet is blocked for transfer");

        BigDecimal sumAfterTransfer = amount.add(amountToTransfer);
        if (sumAfterTransfer.compareTo(maxAmount) > 0) {
            throw new LimitExceededException(this.id, amountToTransfer, amount, "Can't accept money due maximum limit");
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        checkTransfer(amountToTransfer);
        amount = amount.add(amountToTransfer);

    }

    private void checkInconsistency() {
        if (amount.compareTo(maxAmount) > 0)
            throw new IllegalStateException("Inconsistency! " +
                    "Wallet.maxAmount(" + maxAmount + ") must be greater then Wallet.amount(" + amount + ")");
    }


}
