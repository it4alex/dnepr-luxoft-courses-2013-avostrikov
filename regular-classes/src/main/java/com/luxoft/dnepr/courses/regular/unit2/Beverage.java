package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {
    private boolean nonAlcoholic;


    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Beverage)) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        return nonAlcoholic == beverage.nonAlcoholic;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }


    @Override
    public String toString() {
        return super.getCode() + " " + super.getName() + " " + (isNonAlcoholic() ? "Non alcoholic" : "alcoholic") +
                " =  $ " + super.getPrice() + "\n";


    }
}
