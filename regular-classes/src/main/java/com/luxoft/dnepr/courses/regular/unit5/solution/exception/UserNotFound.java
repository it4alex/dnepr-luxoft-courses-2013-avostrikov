package com.luxoft.dnepr.courses.regular.unit5.solution.exception;

public class UserNotFound extends Exception {

    public UserNotFound() { }

    public UserNotFound(String message) {
        super(message);
    }
}
